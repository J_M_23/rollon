// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.model;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import javax.persistence.Table;
import javax.persistence.Entity;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "visitor")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Visitor
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column
    private String visitorId;
    @Column(unique = true)
    private String username;
    @OneToMany(mappedBy = "visitor", cascade = { CascadeType.ALL }, orphanRemoval = true)
    private Set<Answer> answers;
    @OneToMany(mappedBy = "visitor", cascade = { CascadeType.ALL }, orphanRemoval = true)
    private Set<Vote> votes;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String getVisitorId() {
        return this.visitorId;
    }
    
    public void setVisitorId(final String visitorId) {
        this.visitorId = visitorId;
    }
    
    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(final String username) {
        this.username = username;
    }
    
    public Set<Answer> getAnswers() {
        return this.answers;
    }
    
    public void setAnswers(final Set<Answer> answers) {
        this.answers = answers;
    }
    
    public Set<Vote> getVotes() {
        return this.votes;
    }
    
    public void setVotes(final Set<Vote> votes) {
        this.votes = votes;
    }
}
