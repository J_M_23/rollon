// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.model;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import javax.persistence.Table;
import javax.persistence.Entity;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "card")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Card
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column
    private String link;
    @Column
    private String title;
    @Column
    private String description;
    @Column(name = "playlist_name")
    private String playlistName;
    @Column
    private String img;
    @Column
    private Timestamp date_entered;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String getLink() {
        return this.link;
    }
    
    public void setLink(final String link) {
        this.link = link;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(final String description) {
        this.description = description;
    }
    
    public String getPlaylistName() {
        return this.playlistName;
    }
    
    public void setPlaylistName(final String playlistName) {
        this.playlistName = playlistName;
    }
    
    public String getImg() {
        return this.img;
    }
    
    public void setImg(final String img) {
        this.img = img;
    }
    
    public Timestamp getDate_entered() {
        return this.date_entered;
    }
    
    public void setDate_entered(final Timestamp date_entered) {
        this.date_entered = date_entered;
    }
}
