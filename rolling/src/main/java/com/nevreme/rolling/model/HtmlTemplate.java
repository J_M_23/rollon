// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.model;

import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import javax.persistence.Table;
import javax.persistence.Entity;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "html_template")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class HtmlTemplate
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "h_template")
    private String hTemplate;
    @Column
    private String name;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String gethTemplate() {
        return this.hTemplate;
    }
    
    public void sethTemplate(final String hTemplate) {
        this.hTemplate = hTemplate;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
}
