// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.model;

import javax.persistence.ManyToMany;
import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import javax.persistence.Table;
import javax.persistence.Entity;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "playlist")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Playlist
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column
    private String name;
    @Column
    private boolean start;
    @Column
    private String image;
    @Column
    private int playlist_type;
    @OneToMany(mappedBy = "playlist", cascade = { CascadeType.ALL }, orphanRemoval = true)
    private Set<Video> videos;
    @ManyToMany(mappedBy = "playlists")
    private Set<User> users;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public Set<Video> getVideos() {
        return (Set<Video>)this.videos;
    }
    
    public void setVideos(final Set<Video> videos) {
        this.videos = videos;
    }
    
    public boolean isStart() {
        return this.start;
    }
    
    public void setStart(final boolean start) {
        this.start = start;
    }
    
    public String getImage() {
        return this.image;
    }
    
    public void setImage(final String image) {
        this.image = image;
    }
    
    public int getPlaylist_type() {
        return this.playlist_type;
    }
    
    public void setPlaylist_type(final int playlist_type) {
        this.playlist_type = playlist_type;
    }
    
    public Set<User> getUsers() {
        return (Set<User>)this.users;
    }
    
    public void setUsers(final Set<User> users) {
        this.users = users;
    }
    
    @Override
    public String toString() {
        return "Playlist{id=" + this.id + ", name='" + this.name + '\'' + ", start=" + this.start + ", image='" + this.image + '\'' + ", playlist_type=" + this.playlist_type + ", videos=" + this.videos + ", users=" + this.users + '}';
    }
}
