// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.model;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import javax.persistence.Table;
import javax.persistence.Entity;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "vote")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Vote
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    private boolean vote;
    @ManyToOne
    @JoinColumn(name = "visitor_id")
    private Visitor visitor;
    @ManyToOne
    @JoinColumn(name = "answer_id")
    private Answer answer;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public boolean isVote() {
        return this.vote;
    }
    
    public void setVote(final boolean vote) {
        this.vote = vote;
    }
    
    public Visitor getVisitor() {
        return this.visitor;
    }
    
    public void setVisitor(final Visitor visitor) {
        this.visitor = visitor;
    }
    
    public Answer getAnswer() {
        return this.answer;
    }
    
    public void setAnswer(final Answer answer) {
        this.answer = answer;
    }
}
