// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.model;

import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import java.util.Set;
import javax.persistence.Column;
import java.sql.Timestamp;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Entity;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "video", indexes = { @Index(columnList = "playlist_id", name = "idx_playlist_id"), @Index(columnList = "state", name = "idx_state") })
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Video
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String description;
    private Timestamp started;
    private Timestamp recommendDate;
    @Column(name = "yt_id")
    private String ytId;
    private int duration;
    private int index_num;
    private int state;
    private String quote;
    @Column(name = "daily_recommend")
    private int dailyRecommend;
    @Column(name = "video_offset")
    private int offset;
    @ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    @JoinTable(name = "post_tag", joinColumns = { @JoinColumn(name = "post_id") }, inverseJoinColumns = { @JoinColumn(name = "tag_id") })
    private Set<Tag> tags;
    @ManyToOne
    @JoinColumn(name = "playlist_id")
    private Playlist playlist;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(final String description) {
        this.description = description;
    }
    
    public Timestamp getStarted() {
        return this.started;
    }
    
    public void setStarted(final Timestamp started) {
        this.started = started;
    }
    
    public String getYtId() {
        return this.ytId;
    }
    
    public void setYtId(final String ytId) {
        this.ytId = ytId;
    }
    
    public int getDuration() {
        return this.duration;
    }
    
    public void setDuration(final int duration) {
        this.duration = duration;
    }
    
    public int getIndex_num() {
        return this.index_num;
    }
    
    public void setIndex_num(final int index_num) {
        this.index_num = index_num;
    }
    
    public int getState() {
        return this.state;
    }
    
    public void setState(final int state) {
        this.state = state;
    }
    
    public Playlist getPlaylist() {
        return this.playlist;
    }
    
    public void setPlaylist(final Playlist playlist) {
        this.playlist = playlist;
    }
    
    public String getQuote() {
        return this.quote;
    }
    
    public void setQuote(final String quote) {
        this.quote = quote;
    }
    
    public int getOffset() {
        return this.offset;
    }
    
    public void setOffset(final int offset) {
        this.offset = offset;
    }
    
    public int getDailyRecommend() {
        return this.dailyRecommend;
    }
    
    public void setDailyRecommend(final int dailyRecommend) {
        this.dailyRecommend = dailyRecommend;
    }
    
    public Set<Tag> getTags() {
        return this.tags;
    }
    
    public void setTags(final Set<Tag> tags) {
        this.tags = tags;
    }
    
    public Timestamp getRecommendDate() {
        return this.recommendDate;
    }
    
    public void setRecommendDate(final Timestamp recommendDate) {
        this.recommendDate = recommendDate;
    }
}
