// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.model;

import javax.persistence.FetchType;
import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import java.util.Set;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import javax.persistence.Table;
import javax.persistence.Entity;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "answer")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Answer
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(columnDefinition = "TEXT")
    private String text;
    @Column
    private int upVotes;
    @Column
    private int downVotes;
    private Timestamp date;
    @Column(columnDefinition = "TEXT")
    private String html;
    @JoinColumn(name = "reply_answer")
    @ManyToOne
    private Answer replyAnswer;
    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question question;
    @ManyToOne
    @JoinColumn(name = "visitor_id")
    private Visitor visitor;
    @OneToMany(mappedBy = "answer", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Vote> votes;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String getText() {
        return this.text;
    }
    
    public void setText(final String text) {
        this.text = text;
    }
    
    public int getUpVotes() {
        return this.upVotes;
    }
    
    public void setUpVotes(final int upVotes) {
        this.upVotes = upVotes;
    }
    
    public int getDownVotes() {
        return this.downVotes;
    }
    
    public void setDownVotes(final int downVotes) {
        this.downVotes = downVotes;
    }
    
    public Answer getReplyAnswer() {
        return this.replyAnswer;
    }
    
    public void setReplyAnswer(final Answer replyAnswer) {
        this.replyAnswer = replyAnswer;
    }
    
    public Question getQuestion() {
        return this.question;
    }
    
    public void setQuestion(final Question question) {
        this.question = question;
    }
    
    public Visitor getVisitor() {
        return this.visitor;
    }
    
    public void setVisitor(final Visitor visitor) {
        this.visitor = visitor;
    }
    
    public Set<Vote> getVotes() {
        return (Set<Vote>)this.votes;
    }
    
    public void setVotes(final Set<Vote> votes) {
        this.votes = votes;
    }
    
    public Timestamp getDate() {
        return this.date;
    }
    
    public void setDate(final Timestamp date) {
        this.date = date;
    }
    
    public String getHtml() {
        return this.html;
    }
    
    public void setHtml(final String html) {
        this.html = html;
    }
}
