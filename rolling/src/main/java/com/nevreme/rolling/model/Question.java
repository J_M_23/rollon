// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.model;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import javax.persistence.Table;
import javax.persistence.Entity;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "question")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Question
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column
    private String name;
    @Column
    private int active;
    @Column
    private String image;
    @OneToMany(mappedBy = "question", cascade = { CascadeType.ALL }, orphanRemoval = true)
    private Set<Answer> answers;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getImage() {
        return this.image;
    }
    
    public void setImage(final String image) {
        this.image = image;
    }
    
    public Set<Answer> getAnswers() {
        return this.answers;
    }
    
    public void setAnswers(final Set<Answer> answers) {
        this.answers = answers;
    }
    
    public int getActive() {
        return this.active;
    }
    
    public void setActive(final int active) {
        this.active = active;
    }
}
