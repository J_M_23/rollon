// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.model;

import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import javax.persistence.Table;
import javax.persistence.Entity;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "link_suggestion")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class LinkSuggestion
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column
    private String link;
    @Column
    private int linkType;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String getLink() {
        return this.link;
    }
    
    public void setLink(final String link) {
        this.link = link;
    }
    
    public int getLinkType() {
        return this.linkType;
    }
    
    public void setLinkType(final int linkType) {
        this.linkType = linkType;
    }
}
