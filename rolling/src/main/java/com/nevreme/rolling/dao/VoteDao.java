// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dao;

import java.io.Serializable;
import javax.persistence.TypedQuery;
import com.nevreme.rolling.dao.sql.SqlBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.nevreme.rolling.model.Vote;

@Repository
public class VoteDao extends AbstractDao<Vote, Long>
{
    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    public VoteDao(final Vote clazz) {
        super(clazz);
    }
    
    public Long count() {
        return null;
    }
    
    public boolean exists(final Long primaryKey) {
        return false;
    }
    
    public Vote findVoteByVisitorAndAnswer(final Long visitorId, final Long answerId) {
        final TypedQuery<Vote> tq = (TypedQuery<Vote>)this.entityManager.createQuery(new SqlBuilder().select((Class)Vote.class, true).wheres(new String[] { "visitor.id", "answer.id" }, new String[] { "visitorId", "answerId" }).build(), (Class)Vote.class);
        tq.setParameter("visitorId", (Object)visitorId);
        tq.setParameter("answerId", (Object)answerId);
        return tq.getResultList().isEmpty() ? null : ((Vote)tq.getSingleResult());
    }
}
