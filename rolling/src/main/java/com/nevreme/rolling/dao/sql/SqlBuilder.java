// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dao.sql;

import java.util.Arrays;
import java.util.Iterator;
import org.json.JSONObject;

public class SqlBuilder
{
    private String select;
    private String where;
    private String fetch;
    private String insert;
    private String update;
    private String order;
    private String valuesNames;
    private String values;
    
    public SqlBuilder() {
        this.select = "";
        this.where = "";
        this.fetch = "";
        this.insert = "";
        this.update = "";
        this.order = "";
        this.valuesNames = "";
        this.values = "";
    }
    
    public <T> SqlBuilder select(final Class<T> clazz, final boolean distinct) {
        final String distinctText = distinct ? "DISTINCT(t)" : "t";
        this.select = "Select " + distinctText + " from " + clazz.getSimpleName() + " t ";
        return this;
    }
    
    public SqlBuilder wheres(final String[] id, final String[] params) {
        this.where = "WHERE ";
        String and = "";
        for (int i = 0; i < id.length; ++i) {
            and = ((i > 0) ? "AND" : "");
            this.where = this.where + and + " t." + id[i] + " = (:" + params[i] + ")";
        }
        return this;
    }
    
    public SqlBuilder where(final String id, final String... altId) {
        this.where = "WHERE t." + id + " = (:" + ((altId.length > 0) ? altId[0] : id) + ")";
        return this;
    }
    
    public SqlBuilder order(final String field, final String sortType) {
        this.order = " ORDER BY t." + field + " " + sortType;
        return this;
    }
    
    public SqlBuilder fetch(final String name) {
        this.fetch = this.fetch + "LEFT JOIN FETCH t." + name + " ";
        return this;
    }
    
    public String build() {
        return this.select + this.fetch + this.where + this.order;
    }
    
    public <T> SqlBuilder insert(final Class<T> clazz) {
        final String insert = "INSERT INTO " + clazz.getSimpleName().toLowerCase() + " (";
        this.insert = insert;
        return this;
    }
    
    public <T> SqlBuilder update(final Class<T> clazz) {
        final String update = "UPDATE " + clazz.getSimpleName().toLowerCase() + " SET ";
        this.update = update;
        return this;
    }
    
    public SqlBuilder updateValues(final String json, final String id) {
        final JSONObject object = new JSONObject(json);
        String sentence = "";
        System.out.println("::::::::::::::::");
        for (final String key : object.keySet()) {
            System.out.println(key + ",");
            System.out.println(object.get(key));
            sentence = sentence + key + "='" + object.get(key) + "',";
        }
        sentence = sentence.substring(0, sentence.length() - 1);
        this.update = this.update + sentence + "WHERE id = " + id;
        return this;
    }
    
    public SqlBuilder values(final String json, String apostrophe) {
        final JSONObject object = new JSONObject(json);
        String names = "";
        String values = "";
        System.out.println("::::::::::::::::");
        for (final String key : object.keySet()) {
            System.out.println(key + ",");
            System.out.println(object.get(key));
            names = names + key + ",";
            apostrophe = (object.get(key).getClass().getSimpleName().equals("String") ? "'" : "");
            values = values + apostrophe + object.get(key) + apostrophe + ",";
        }
        System.out.println("::::::::::::::::");
        names = names.substring(0, names.length() - 1);
        values = values.substring(0, values.length() - 1);
        this.valuesNames = names + ")";
        this.values = " VALUES (" + values + ")";
        return this;
    }
    
    public String insertBuild() {
        return this.insert + this.valuesNames + this.values;
    }
    
    public String updateBuild() {
        return this.update;
    }
    
    public Object[] getValues(final String json) {
        final JSONObject object = new JSONObject(json);
        final Iterator<String> keys = (Iterator<String>)object.keys();
        final Object[] values = new Object[object.keySet().size()];
        int i = 0;
        while (keys.hasNext()) {
            values[i] = object.get((String)keys.next());
            ++i;
        }
        return values;
    }
    
    public String getKeys(final String json) {
        final JSONObject object = new JSONObject(json);
        final Iterator<String> keys = (Iterator<String>)object.keys();
        final Object[] names = new Object[object.keySet().size()];
        int i = 0;
        while (keys.hasNext()) {
            names[i] = keys.next();
            ++i;
        }
        return Arrays.toString(names).replace("[", "").replace("]", "");
    }
    
    public <T> String insertQuery(final Class<T> clazz, final String json) {
        final int size = new JSONObject(json).keySet().size();
        String questionMarks = "";
        for (int i = 0; i < size; ++i) {
            questionMarks += "?,";
        }
        questionMarks = questionMarks.substring(0, questionMarks.length() - 1);
        final String query = "Insert into " + clazz.getSimpleName().toLowerCase() + " (" + this.getKeys(json) + ") VALUES (" + questionMarks + ")";
        return query;
    }
}
