// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dao;

import java.util.List;
import javax.persistence.TypedQuery;
import com.nevreme.rolling.dao.sql.SqlBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.nevreme.rolling.model.Video;

@Repository
public class VideoDao extends AbstractDao<Video, Long>
{
    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    public VideoDao(final Video clazz) {
        super(clazz);
    }
    
    public Video findVideoByState(final int state) {
        final TypedQuery<Video> tq = (TypedQuery<Video>)this.entityManager.createQuery(new SqlBuilder().select((Class)Video.class, true).where("state", new String[0]).build(), (Class)Video.class);
        tq.setParameter("state", (Object)state);
        return (Video)tq.getSingleResult();
    }
    
    public Video findVideoByStateForPlaylist(final int state, final Long playlist_id) {
        final TypedQuery<Video> tq = (TypedQuery<Video>)this.entityManager.createQuery(new SqlBuilder().select((Class)Video.class, true).wheres(new String[] { "state", "playlist.id" }, new String[] { "state", "playlist_id" }).build(), (Class)Video.class);
        tq.setParameter("state", (Object)state);
        tq.setParameter("playlist_id", (Object)playlist_id);
        return (Video)tq.getSingleResult();
    }
    
    public Video findNextVideo(int index) {
        final TypedQuery<Video> tq = (TypedQuery<Video>)this.entityManager.createQuery(new SqlBuilder().select((Class)Video.class, true).where("index_num", new String[0]).build(), (Class)Video.class);
        tq.setParameter("index_num", (Object)(++index));
        if (tq.getResultList() == null || tq.getResultList().size() == 0) {
            tq.setParameter("index_num", (Object)1);
        }
        return (Video)tq.getSingleResult();
    }
    
    public Video findNextVideoForPlaylist(int index, final Long playlist_id) {
        final TypedQuery<Video> tq = (TypedQuery<Video>)this.entityManager.createQuery(new SqlBuilder().select((Class)Video.class, true).wheres(new String[] { "index_num", "playlist.id" }, new String[] { "index_num", "playlist_id" }).build(), (Class)Video.class);
        tq.setParameter("index_num", (Object)(++index));
        tq.setParameter("playlist_id", (Object)playlist_id);
        if (tq.getResultList() == null || tq.getResultList().size() == 0) {
            tq.setParameter("index_num", (Object)1);
            tq.setParameter("playlist_id", (Object)playlist_id);
        }
        return (Video)tq.getSingleResult();
    }
    
    public Video findVideoByYtId(final String ytId, final Long plId) {
        final TypedQuery<Video> tq = (TypedQuery<Video>)this.entityManager.createQuery(new SqlBuilder().select((Class)Video.class, true).wheres(new String[] { "ytId", "playlist.id" }, new String[] { "ytId", "playlist_id" }).build(), (Class)Video.class);
        tq.setParameter("ytId", (Object)ytId);
        tq.setParameter("playlist_id", (Object)plId);
        return (Video)tq.getSingleResult();
    }
    
    public void insertNewVideo(final Video video) {
        final Video latestVideo = (Video)this.entityManager.createQuery(new SqlBuilder().select((Class)Video.class, true).order("index_num", "DESC").wheres(new String[] { "playlist.id" }, new String[] { "playlist_id" }).build(), (Class)Video.class).setParameter("playlist_id", (Object)video.getPlaylist().getId()).setMaxResults(1).getSingleResult();
        video.setIndex_num(latestVideo.getIndex_num() + 1);
        this.entityManager.flush();
        this.entityManager.merge((Object)video);
    }
    
    public List<Video> updateAllHigherThan(final int index, final Long playlist) {
        final List<Video> videos = (List<Video>)this.entityManager.createQuery("SELECT v FROM Video v WHERE v.index_num > (:index) AND v.playlist.id = (:playlist)", (Class)Video.class).setParameter("index", (Object)index).setParameter("playlist", (Object)playlist).getResultList();
        for (final Video video : videos) {
            video.setIndex_num(video.getIndex_num() - 1);
            this.entityManager.merge((Object)video);
        }
        return videos;
    }
    
    public List<Video> findVideosForPlaylistByRange(final Long plId, final int start, final int end) {
        final TypedQuery<Video> tq = (TypedQuery<Video>)this.entityManager.createQuery("SELECT v FROM Video v WHERE v.playlist.id = (:playlist) ORDER BY v.index_num DESC", (Class)Video.class);
        tq.setParameter("playlist", (Object)plId);
        tq.setFirstResult(start);
        tq.setMaxResults(end);
        return (List<Video>)tq.getResultList();
    }
    
    public void updateList(final List<Video> videos) {
        for (final Video video : videos) {
            this.entityManager.merge((Object)video);
        }
    }
    
    public Long count() {
        return null;
    }
    
    public boolean exists(final Long primaryKey) {
        return false;
    }
    
    public Video findRecommendedVideoByPlaylist(final int playlist_type) {
        final TypedQuery<Video> tq = (TypedQuery<Video>)this.entityManager.createQuery("SELECT v FROM Video v WHERE v.playlist.playlist_type = (:type) AND v.dailyRecommend = (:daily)", (Class)Video.class);
        tq.setParameter("daily", (Object)1);
        tq.setParameter("type", (Object)playlist_type);
        return (Video)tq.getSingleResult();
    }
}
