// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dao;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.nevreme.rolling.model.HtmlTemplate;

@Repository
public class HtmlTemplateDao extends AbstractDao<HtmlTemplate, Long>
{
    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    public HtmlTemplateDao(final HtmlTemplate clazz) {
        super(clazz);
    }
    
    public Long count() {
        return null;
    }
    
    public boolean exists(final Long primaryKey) {
        return false;
    }
}
