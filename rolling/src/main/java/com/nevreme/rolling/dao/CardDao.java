// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dao;

import java.io.Serializable;
import com.nevreme.rolling.dao.sql.SqlBuilder;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.nevreme.rolling.model.Card;

@Repository
public class CardDao extends AbstractDao<Card, Long>
{
    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    public CardDao(final Card clazz) {
        super(clazz);
    }
    
    public Long count() {
        return null;
    }
    
    public List<Card> findAllInRange(final int start, final int max) {
        final String sql = new SqlBuilder().select((Class)Card.class, true).order("date_entered", "DESC").build();
        return (List<Card>)this.entityManager.createQuery(sql, (Class)Card.class).setFirstResult(start).setMaxResults(max).getResultList();
    }
    
    public boolean exists(final Long primaryKey) {
        return false;
    }
}
