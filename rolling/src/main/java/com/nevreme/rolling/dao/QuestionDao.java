// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.TypedQuery;
import com.nevreme.rolling.dao.sql.SqlBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.nevreme.rolling.model.Question;

@Repository
public class QuestionDao extends AbstractDao<Question, Long>
{
    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    public QuestionDao(final Question clazz) {
        super(clazz);
    }
    
    public Long count() {
        return null;
    }
    
    public boolean exists(final Long primaryKey) {
        return false;
    }
    
    public Question findOneEagerly(final Long primaryKey) {
        final TypedQuery<Question> tq = (TypedQuery<Question>)this.entityManager.createQuery(new SqlBuilder().select((Class)Question.class, true).fetch("answers").where("id", new String[0]).build(), (Class)Question.class);
        tq.setParameter("id", (Object)primaryKey);
        return (Question)tq.getSingleResult();
    }
    
    public Question findOneEagerlyActive() {
        final TypedQuery<Question> tq = (TypedQuery<Question>)this.entityManager.createQuery(new SqlBuilder().select((Class)Question.class, true).fetch("answers").where("active", new String[0]).build(), (Class)Question.class);
        tq.setParameter("active", (Object)1);
        return (Question)tq.getSingleResult();
    }
    
    public List<Question> findAllInRange(final int start, final int max) {
        final String sql = new SqlBuilder().select((Class)Question.class, true).order("date", "DESC").build();
        return (List<Question>)this.entityManager.createQuery(sql, (Class)Question.class).setFirstResult(start).setMaxResults(max).getResultList();
    }
    
    public List<Question> findAllEagerly() {
        final String sql = new SqlBuilder().select((Class)Question.class, true).fetch("answers").build();
        return (List<Question>)this.entityManager.createQuery(sql, (Class)Question.class).getResultList();
    }
}
