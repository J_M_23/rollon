// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.TypedQuery;
import com.nevreme.rolling.dao.sql.SqlBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.nevreme.rolling.model.Visitor;

@Repository
public class VisitorDao extends AbstractDao<Visitor, Long>
{
    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    public VisitorDao(final Visitor clazz) {
        super(clazz);
    }
    
    public Long count() {
        return null;
    }
    
    public boolean exists(final Long primaryKey) {
        return false;
    }
    
    public Visitor findOneEagerly(final Long primaryKey) {
        final String sql = new SqlBuilder().select((Class)Visitor.class, true).fetch("answers").build();
        return (Visitor)this.entityManager.createQuery(sql, (Class)Visitor.class).getSingleResult();
    }
    
    public Visitor findByVisitorId(final String id) {
        final TypedQuery<Visitor> tq = (TypedQuery<Visitor>)this.entityManager.createQuery(new SqlBuilder().select((Class)Visitor.class, true).where("visitorId", new String[0]).build(), (Class)Visitor.class);
        tq.setParameter("visitorId", (Object)id);
        return tq.getResultList().isEmpty() ? null : ((Visitor)tq.getSingleResult());
    }
    
    public List<Visitor> findAllEagerly() {
        final String sql = new SqlBuilder().select((Class)Visitor.class, true).fetch("answers").build();
        return (List<Visitor>)this.entityManager.createQuery(sql, (Class)Visitor.class).getResultList();
    }
}
