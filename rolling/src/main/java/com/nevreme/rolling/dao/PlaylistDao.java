// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.TypedQuery;
import com.nevreme.rolling.dao.sql.SqlBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.nevreme.rolling.model.Playlist;

@Repository
public class PlaylistDao extends AbstractDao<Playlist, Long>
{
    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    public PlaylistDao(final Playlist clazz) {
        super(clazz);
    }
    
    public Long getPlaylistByName(final String name) {
        final TypedQuery<Playlist> tq = (TypedQuery<Playlist>)this.entityManager.createQuery(new SqlBuilder().select((Class)Playlist.class, true).where("name", new String[0]).build(), (Class)Playlist.class);
        tq.setParameter("name", (Object)name);
        return ((Playlist)tq.getSingleResult()).getId();
    }
    
    public Playlist findOneEagerly(final Long primaryKey) {
        final String sql = new SqlBuilder().select((Class)Playlist.class, true).fetch("tags").fetch("videos").where("id", new String[0]).build();
        final TypedQuery<Playlist> tq = (TypedQuery<Playlist>)this.entityManager.createQuery(sql, (Class)Playlist.class);
        tq.setParameter("id", (Object)primaryKey);
        return (Playlist)tq.getSingleResult();
    }
    
    public Playlist findOneNoTags(final Long primaryKey) {
        final String sql = new SqlBuilder().select((Class)Playlist.class, true).fetch("videos").where("id", new String[0]).build();
        final TypedQuery<Playlist> tq = (TypedQuery<Playlist>)this.entityManager.createQuery(sql, (Class)Playlist.class);
        tq.setParameter("id", (Object)primaryKey);
        return (Playlist)tq.getSingleResult();
    }
    
    public Long count() {
        return null;
    }
    
    public boolean exists(final Long primaryKey) {
        return false;
    }
    
    public List<Playlist> getPLaylstByType(final int type) {
        final TypedQuery<Playlist> tq = (TypedQuery<Playlist>)this.entityManager.createQuery(new SqlBuilder().select((Class)Playlist.class, true).fetch("videos").fetch("users").where("playlist_type", new String[0]).build(), (Class)Playlist.class);
        tq.setParameter("playlist_type", (Object)type);
        return (List<Playlist>)tq.getResultList();
    }
    
    public List<Playlist> getPLaylstByTypeInRange(final int type, final int start, final int end) {
        final TypedQuery<Playlist> tq = (TypedQuery<Playlist>)this.entityManager.createQuery(new SqlBuilder().select((Class)Playlist.class, true).fetch("videos").fetch("users").where("playlist_type", new String[0]).build(), (Class)Playlist.class);
        tq.setParameter("playlist_type", (Object)type);
        tq.setFirstResult(start);
        tq.setMaxResults(end);
        return (List<Playlist>)tq.getResultList();
    }
    
    public List<Playlist> getPLaylstByTypeLazy(final int type) {
        final TypedQuery<Playlist> tq = (TypedQuery<Playlist>)this.entityManager.createQuery(new SqlBuilder().select((Class)Playlist.class, true).where("playlist_type", new String[0]).build(), (Class)Playlist.class);
        tq.setParameter("playlist_type", (Object)type);
        return (List<Playlist>)tq.getResultList();
    }
}
