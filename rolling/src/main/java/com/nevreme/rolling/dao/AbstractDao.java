// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dao;

import javax.persistence.Query;
import java.util.List;
import com.nevreme.rolling.dao.sql.SqlBuilder;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import java.io.Serializable;

public abstract class AbstractDao<T, ID extends Serializable>
{
    @PersistenceContext
    private EntityManager entityManager;
    private T clazz;
    
    public AbstractDao(final T clazz) {
        this.clazz = clazz;
    }
    
    public T save(final T entity) {
        return (T)this.entityManager.merge((Object)entity);
    }
    
    public T update(final T entity) {
        return (T)this.entityManager.merge((Object)entity);
    }
    
    public T findOne(final ID primaryKey) {
        final String sql = new SqlBuilder().select((Class)this.clazz.getClass(), true).where("id", new String[0]).build();
        final T t = (T)this.entityManager.createQuery(sql, (Class)this.clazz.getClass()).setParameter("id", (Object)primaryKey).getSingleResult();
        return t;
    }
    
    public T findOneEagerly(final ID primaryKey) {
        final String sql = new SqlBuilder().select((Class)this.clazz.getClass(), true).where("id", new String[0]).build();
        final T t = (T)this.entityManager.createQuery(sql, (Class)this.clazz.getClass()).setParameter("id", (Object)primaryKey).getSingleResult();
        return t;
    }
    
    public List<T> findAllEagerly() {
        final String sql = new SqlBuilder().select((Class)this.clazz.getClass(), true).build();
        final List<T> t = (List<T>)this.entityManager.createQuery(sql, (Class)this.clazz.getClass()).getResultList();
        return t;
    }
    
    public List<T> findAll() {
        final String sql = new SqlBuilder().select((Class)this.clazz.getClass(), true).build();
        final List<T> t = (List<T>)this.entityManager.createQuery(sql, (Class)this.clazz.getClass()).getResultList();
        return t;
    }
    
    public void delete(final ID id) {
        final T entity = (T)this.findOne(id);
        System.out.println("nasaooooooooo " + entity);
        this.entityManager.remove((Object)entity);
    }
    
    public void saveWithSql(final String sql) {
        final SqlBuilder sqlBuilder = new SqlBuilder();
        final Object[] values = sqlBuilder.getValues(sql);
        final Query query = this.entityManager.createNativeQuery(sqlBuilder.insertQuery((Class)this.clazz.getClass(), sql));
        for (int i = 0; i < values.length; ++i) {
            query.setParameter(i + 1, (Object)("" + values[i] + ""));
        }
        query.executeUpdate();
    }
    
    public void updateWithSql(final String sql, final String id) {
        final String query = new SqlBuilder().update((Class)this.clazz.getClass()).updateValues(sql, id).updateBuild();
        System.out.println(query);
        this.entityManager.createNativeQuery(query).executeUpdate();
    }
    
    public abstract Long count();
    
    public abstract boolean exists(final ID p0);
}
