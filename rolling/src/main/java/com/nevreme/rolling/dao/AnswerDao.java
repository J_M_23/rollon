// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dao;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.nevreme.rolling.model.Answer;

@Repository
public class AnswerDao extends AbstractDao<Answer, Long>
{
    @Autowired
    public AnswerDao(final Answer clazz) {
        super(clazz);
    }
    
    public Long count() {
        return null;
    }
    
    public boolean exists(final Long primaryKey) {
        return false;
    }
}
