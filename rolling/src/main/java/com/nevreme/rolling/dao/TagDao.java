// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dao;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.nevreme.rolling.model.Tag;

@Repository
public class TagDao extends AbstractDao<Tag, Long>
{
    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    public TagDao(final Tag clazz) {
        super(clazz);
    }
    
    public Long count() {
        return null;
    }
    
    public boolean exists(final Long primaryKey) {
        return false;
    }
}
