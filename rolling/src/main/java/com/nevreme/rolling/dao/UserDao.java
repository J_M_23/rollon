// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dao;

import java.io.Serializable;
import java.util.List;
import com.nevreme.rolling.dao.sql.SqlBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.nevreme.rolling.model.User;

@Repository
public class UserDao extends AbstractDao<User, Long>
{
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    @Autowired
    public UserDao(final User clazz) {
        super(clazz);
    }
    
    public void flush() {
        this.entityManager.flush();
    }
    
    public void clear() {
        this.entityManager.clear();
    }
    
    public User save(final User admin) {
        admin.setPassword(this.bCryptPasswordEncoder.encode((CharSequence)admin.getPassword()));
        admin.setActive(1);
        return (User)this.entityManager.merge((Object)admin);
    }
    
    public User findUserByEmail(final String email) {
        final String sql = new SqlBuilder().select((Class)User.class, true).fetch("roles").where("username", new String[0]).build();
        final List<User> users = (List<User>)this.entityManager.createQuery(sql, (Class)User.class).setParameter("username", (Object)email).getResultList();
        if (users.isEmpty()) {
            return null;
        }
        return users.get(0);
    }
    
    public Long count() {
        return null;
    }
    
    public void delete(User admin) {
        if (this.entityManager.contains((Object)admin)) {
            this.entityManager.remove((Object)admin);
        }
        else {
            admin = (User)this.findOne(admin.getId());
            this.entityManager.remove((Object)admin);
        }
    }
    
    public boolean exists(final Long primaryKey) {
        return this.findOne(primaryKey) != null;
    }
    
    public User findOneEagerly(final Long primaryKey) {
        if (primaryKey == null) {
            return null;
        }
        final String sql = new SqlBuilder().select((Class)User.class, true).fetch("roles").where("id", new String[0]).build();
        final User admin = (User)this.entityManager.createQuery(sql, (Class)User.class).setParameter("id", (Object)primaryKey).getSingleResult();
        return admin;
    }
    
    public List<User> findAll() {
        final String sql = new SqlBuilder().select((Class)User.class, true).build();
        return (List<User>)this.entityManager.createQuery(sql, (Class)User.class).getResultList();
    }
    
    public List<User> findAllEagerly() {
        final String sql = new SqlBuilder().select((Class)User.class, true).fetch("roles").build();
        return (List<User>)this.entityManager.createQuery(sql, (Class)User.class).getResultList();
    }
    
    public User update(final User admin) {
        final User entity = (User)this.findOne(admin.getId());
        if (entity != null && !admin.getPassword().equals(entity.getPassword())) {
            admin.setPassword(this.bCryptPasswordEncoder.encode((CharSequence)entity.getPassword()));
        }
        return (User)this.entityManager.merge((Object)admin);
    }
}
