// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller.rest;

import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.util.Date;
import java.io.File;
import com.nevreme.rolling.utils.Constants;
import org.springframework.web.bind.annotation.ResponseBody;
import java.io.IOException;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.AbstractService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.nevreme.rolling.dto.UserDto;
import com.nevreme.rolling.model.User;

@Controller
@RequestMapping({ "/admin/api/user/" })
public class UserRestController extends AbstractRestController<User, UserDto, Long>
{
    @Autowired
    public UserRestController(final AbstractService<User, Long> repo, final UserDto dto) {
        super((AbstractService)repo, dto);
    }
    
    @RequestMapping({ "/saveImg" })
    @ResponseBody
    public String upload(@RequestParam("file") final MultipartFile img) throws IOException {
        return this.uploadFileHandler(img, img.getOriginalFilename());
    }
    
    public String uploadFileHandler(final MultipartFile file, final String name) {
        if (!file.isEmpty()) {
            try {
                final byte[] bytes = file.getBytes();
                final File dir = new File(Constants.IMAGE_PATH);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                final Timestamp date = new Timestamp(new Date().getTime());
                final String foldName = date.getYear() + "" + (date.getMonth() + 1) + "" + date.getDate() + "" + date.getMinutes();
                final File dirDate = new File(Constants.IMAGE_PATH + File.separator + foldName);
                if (!dirDate.exists()) {
                    dirDate.mkdirs();
                }
                final File serverFile = new File(dirDate.getAbsolutePath() + File.separator + name);
                final BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();
                System.out.println("Server File Location=" + serverFile.getAbsolutePath());
                return "/res/images/" + foldName + "/" + name;
            }
            catch (Exception e) {
                return "You failed to upload " + name + " => " + e.getMessage();
            }
        }
        return "You failed to upload " + name + " because the file was empty.";
    }
}
