// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller;

import com.nevreme.rolling.model.Video;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import java.io.Serializable;
import com.nevreme.rolling.model.Playlist;
import org.springframework.web.servlet.ModelAndView;
import com.nevreme.rolling.utils.PlaylistUtils;
import com.nevreme.rolling.service.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.VideoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@Controller
@RequestMapping({ "/text" })
public class TextsController
{
    @Autowired
    VideoService videoService;
    @Autowired
    PlaylistService playlistService;
    @Autowired
    PlaylistUtils playlistUtils;
    
    @RequestMapping(value = { "/", "" }, method = { RequestMethod.GET })
    public ModelAndView addPost() {
        final ModelAndView modelAndView = new ModelAndView();
        final Long playlist_id = this.playlistService.getPlaylistByName("Poezija");
        modelAndView.addObject("playlist_id", (Object)playlist_id);
        modelAndView.addObject("vid_id", (Object)(-1));
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.addObject("ws", (Object)((System.getProperty("WS") == null) ? "" : System.getProperty("WS")));
        modelAndView.addObject("m_title", (Object)"Rolling Texts (Poezija)");
        modelAndView.addObject("m_url", (Object)"http://radiorolling.com/text/");
        modelAndView.addObject("m_desc", (Object)"Tekstovi, kratke pri\u010de i poezija");
        modelAndView.addObject("m_image", (Object)((Playlist)this.playlistService.findOne(playlist_id)).getImage());
        modelAndView.setViewName("admin/text");
        return modelAndView;
    }
    
    @RequestMapping(value = { "/{name}/", "/{name}" }, method = { RequestMethod.GET })
    public ModelAndView staticalName(@PathVariable final String name) {
        final ModelAndView modelAndView = new ModelAndView();
        final Long playlist_id = this.playlistService.getPlaylistByName(name);
        modelAndView.addObject("playlist_id", (Object)playlist_id);
        modelAndView.addObject("vid_id", (Object)(-1));
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.addObject("ws", (Object)((System.getProperty("WS") == null) ? "" : System.getProperty("WS")));
        modelAndView.addObject("m_title", (Object)("Rolling Texts (" + name + ")"));
        modelAndView.addObject("m_url", (Object)("http://radiorolling.com/text/" + name));
        modelAndView.addObject("m_desc", (Object)"Tekstovi, kratke pri\u010de i poezija");
        modelAndView.addObject("m_image", (Object)((Playlist)this.playlistService.findOne(playlist_id)).getImage());
        modelAndView.setViewName("admin/text");
        return modelAndView;
    }
    
    @RequestMapping(value = { "/t" }, method = { RequestMethod.GET })
    public ModelAndView staticalVid(@RequestParam final Long text, @RequestParam final String plName) {
        final ModelAndView modelAndView = new ModelAndView();
        final Video video = this.videoService.findOne(text);
        final Long playlist_id = this.playlistService.getPlaylistByName(plName);
        modelAndView.addObject("playlist_id", (Object)playlist_id);
        modelAndView.addObject("vid_id", (Object)text);
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.addObject("ws", (Object)((System.getProperty("WS") == null) ? "" : System.getProperty("WS")));
        modelAndView.addObject("m_title", (Object)("Rolling Texts (" + plName + ")"));
        modelAndView.addObject("m_url", (Object)("http://radiorolling.com/text/t?text=" + text + "&plName=" + plName));
        modelAndView.addObject("m_desc", (Object)video.getDescription());
        modelAndView.addObject("m_image", (Object)video.getYtId());
        modelAndView.setViewName("admin/text");
        return modelAndView;
    }
}
