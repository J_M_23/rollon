// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller;

import com.nevreme.rolling.model.Video;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import java.io.Serializable;
import com.nevreme.rolling.model.Playlist;
import org.springframework.web.servlet.ModelAndView;
import com.nevreme.rolling.utils.PlaylistUtils;
import com.nevreme.rolling.service.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.VideoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@Controller
@RequestMapping({ "/video" })
public class VideoController
{
    @Autowired
    VideoService videoService;
    @Autowired
    PlaylistService playlistService;
    @Autowired
    PlaylistUtils playlistUtils;
    private boolean started;
    
    public VideoController() {
        this.started = false;
    }
    
    @RequestMapping(value = { "autoplay/", "autoplay" }, method = { RequestMethod.GET })
    public ModelAndView addPost() {
        final ModelAndView modelAndView = new ModelAndView();
        final Long playlist_id = this.playlistService.getPlaylistByName("Inserti Iz Filmova");
        modelAndView.addObject("playlist_id", (Object)playlist_id);
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.addObject("ws", (Object)((System.getProperty("WS") == null) ? "" : System.getProperty("WS")));
        modelAndView.addObject("m_title", (Object)"Rolling Video (Inserti Iz Filmova)");
        modelAndView.addObject("m_url", (Object)"http://radiorolling.com/video/autoplay/Inserti Iz Filmova");
        modelAndView.addObject("m_desc", (Object)"Radio Mode");
        modelAndView.addObject("m_image", (Object)((Playlist)this.playlistService.findOne(playlist_id)).getImage());
        modelAndView.setViewName("admin/video");
        return modelAndView;
    }
    
    @RequestMapping(value = { "manualplay/", "manualplay" }, method = { RequestMethod.GET })
    public ModelAndView statical() {
        final ModelAndView modelAndView = new ModelAndView();
        final Long playlist_id = this.playlistService.getPlaylistByName("Inserti Iz Filmova");
        modelAndView.addObject("playlist_id", (Object)playlist_id);
        modelAndView.addObject("vid_id", (Object)(-1));
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.addObject("ws", (Object)((System.getProperty("WS") == null) ? "" : System.getProperty("WS")));
        modelAndView.addObject("m_title", (Object)"Rolling Video (Inserti Iz Filmova)");
        modelAndView.addObject("m_url", (Object)"http://radiorolling.com/video/manualplay/Inserti Iz Filmova");
        modelAndView.addObject("m_desc", (Object)"Manual Mode");
        modelAndView.addObject("m_image", (Object)((Playlist)this.playlistService.findOne(playlist_id)).getImage());
        modelAndView.setViewName("admin/staticalVideo");
        return modelAndView;
    }
    
    @RequestMapping(value = { "manualplay/{name}/", "manualplay/{name}" }, method = { RequestMethod.GET })
    public ModelAndView staticalName(@PathVariable final String name) {
        final ModelAndView modelAndView = new ModelAndView();
        final Long playlist_id = this.playlistService.getPlaylistByName(name);
        modelAndView.addObject("playlist_id", (Object)playlist_id);
        modelAndView.addObject("vid_id", (Object)(-1));
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.addObject("ws", (Object)((System.getProperty("WS") == null) ? "" : System.getProperty("WS")));
        modelAndView.addObject("m_title", (Object)("Rolling Video (" + name + ")"));
        modelAndView.addObject("m_url", (Object)("http://radiorolling.com/video/manualplay/" + name));
        modelAndView.addObject("m_desc", (Object)"Manual Mode");
        modelAndView.addObject("m_image", (Object)((Playlist)this.playlistService.findOne(playlist_id)).getImage());
        modelAndView.setViewName("admin/staticalVideo");
        return modelAndView;
    }
    
    @RequestMapping(value = { "manualplay/vid" }, method = { RequestMethod.GET })
    public ModelAndView staticalVid(@RequestParam final String vid, @RequestParam final String plName) {
        final ModelAndView modelAndView = new ModelAndView();
        final Long playlist_id = this.playlistService.getPlaylistByName(plName);
        final Video video = this.videoService.findVideoByYtId(vid, playlist_id);
        modelAndView.addObject("playlist_id", (Object)playlist_id);
        modelAndView.addObject("vid_id", (Object)vid);
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.addObject("ws", (Object)((System.getProperty("WS") == null) ? "" : System.getProperty("WS")));
        modelAndView.addObject("m_title", (Object)("Rolling Video (" + plName + ")"));
        modelAndView.addObject("m_url", (Object)("http://radiorolling.com/video/manualplay/vid?vid=" + vid + "&plName=" + plName));
        modelAndView.addObject("m_desc", (Object)video.getDescription());
        modelAndView.addObject("m_image", (Object)("https://i.ytimg.com/vi/" + vid + "/hqdefault.jpg"));
        modelAndView.setViewName("admin/staticalVideo");
        return modelAndView;
    }
    
    @RequestMapping(value = { "autoplay/{name}/", "autoplay/{name}" }, method = { RequestMethod.GET })
    public ModelAndView ids(@PathVariable final String name) {
        final ModelAndView modelAndView = new ModelAndView();
        final Long playlist_id = this.playlistService.getPlaylistByName(name);
        modelAndView.addObject("playlist_id", (Object)playlist_id);
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.addObject("ws", (Object)((System.getProperty("WS") == null) ? "" : System.getProperty("WS")));
        modelAndView.addObject("m_title", (Object)("Rolling Video (" + name + ")"));
        modelAndView.addObject("m_url", (Object)("http://radiorolling.com/video/autoplay/" + name));
        modelAndView.addObject("m_desc", (Object)"Radio Mode");
        modelAndView.addObject("m_image", (Object)((Playlist)this.playlistService.findOne(playlist_id)).getImage());
        modelAndView.setViewName("admin/video");
        return modelAndView;
    }
}
