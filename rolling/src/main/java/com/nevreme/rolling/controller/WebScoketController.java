// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import java.sql.Timestamp;
import java.util.Date;
import java.io.Serializable;
import com.nevreme.rolling.model.Playlist;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.handler.annotation.MessageMapping;
import com.nevreme.rolling.model.Video;
import com.nevreme.rolling.dto.VideoDetailsDto;
import org.springframework.messaging.handler.annotation.Payload;
import com.nevreme.rolling.dto.RequestMessageDto;
import com.nevreme.rolling.utils.PlaylistUtils;
import com.nevreme.rolling.service.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.VideoService;
import org.springframework.stereotype.Controller;

@Controller
public class WebScoketController
{
    @Autowired
    VideoService videoService;
    @Autowired
    PlaylistService service;
    @Autowired
    PlaylistUtils playlistUtils;
    
    @MessageMapping({ "/radio.nowplaying" })
    @SendTo({ "/channel/public" })
    public VideoDetailsDto sendMessage(@Payload final RequestMessageDto requestMessage) {
        final Video video = this.videoService.findVideoByState(0);
        long time = 0L;
        final long currentMilliseconds = System.currentTimeMillis();
        time = currentMilliseconds / 1000L - video.getStarted().getTime() / 1000L;
        final VideoDetailsDto videoDto = new VideoDetailsDto();
        videoDto.setVideoDescription(video.getDescription());
        videoDto.setVideoDuration("" + video.getDuration());
        videoDto.setVideoUrl(video.getYtId() + "?start=" + time);
        videoDto.setVideoOffset(video.getOffset());
        return videoDto;
    }
    
    @CrossOrigin({ "*" })
    @RequestMapping({ "/public/nowPlaying/{playlistId}" })
    @ResponseBody
    public VideoDetailsDto getVideo(@PathVariable final Long playlistId) {
        final Video video = this.videoService.findVideoByStateForPlaylist(0, playlistId);
        final Playlist pl = (Playlist)this.service.findOne(playlistId);
        long time = 0L;
        if (!pl.isStart()) {
            pl.setStart(true);
            this.service.save(pl);
            this.playlistUtils.executeJob(1000L * (video.getDuration() - time), playlistId);
            video.setStarted(new Timestamp(new Date().getTime()));
            this.videoService.save(video);
        }
        final long currentMilliseconds = System.currentTimeMillis();
        time = currentMilliseconds / 1000L - video.getStarted().getTime() / 1000L;
        final VideoDetailsDto videoDto = new VideoDetailsDto();
        videoDto.setVideoDescription(video.getDescription());
        videoDto.setVideoDuration("" + video.getDuration());
        videoDto.setVideoUrl(video.getYtId() + "?start=" + time);
        videoDto.setVideoQuote(video.getQuote());
        videoDto.setVideoOffset(video.getOffset());
        videoDto.setPlId(video.getPlaylist().getId().toString());
        return videoDto;
    }
    
    @CrossOrigin({ "*" })
    @RequestMapping({ "/public/nowPlaying/all" })
    @ResponseBody
    public List<VideoDetailsDto> getNowAll() {
        final List<Playlist> pls = (List<Playlist>)this.service.getPLaylstByTypeLazy(1);
        final List<VideoDetailsDto> videoDtos = new ArrayList<VideoDetailsDto>();
        for (final Playlist p : pls) {
            final Video video = this.videoService.findVideoByStateForPlaylist(0, p.getId());
            long time = 0L;
            final long currentMilliseconds = System.currentTimeMillis();
            time = currentMilliseconds / 1000L - video.getStarted().getTime() / 1000L;
            final VideoDetailsDto videoDto = new VideoDetailsDto();
            videoDto.setVideoDescription(video.getDescription());
            videoDto.setVideoDuration("" + video.getDuration());
            videoDto.setVideoUrl(video.getYtId() + "?start=" + time);
            videoDto.setVideoQuote(p.getName());
            videoDto.setVideoOffset(video.getOffset());
            videoDto.setPlId(video.getPlaylist().getId().toString());
            videoDtos.add(videoDto);
        }
        return videoDtos;
    }
}
