// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller.rest;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.nevreme.rolling.model.Playlist;
import java.io.Serializable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import com.nevreme.rolling.service.AbstractService;
import com.nevreme.rolling.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.PlaylistService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.nevreme.rolling.dto.UserDto;
import com.nevreme.rolling.model.User;

@Controller
@RequestMapping({ "/editor/api/" })
public class EditorRestController extends AbstractRestController<User, UserDto, Long>
{
    @Autowired
    PlaylistService playlistService;
    @Autowired
    UserService userService;
    
    @Autowired
    public EditorRestController(final AbstractService<User, Long> repo, final UserDto dto) {
        super((AbstractService)repo, dto);
    }
    
    @CrossOrigin(origins = { "*" })
    @RequestMapping(value = { "/add" }, method = { RequestMethod.POST })
    @ResponseBody
    public String addPlaylist(@RequestBody final Long[] playlists, @RequestParam(name = "userId") final Long userId) {
        final User user = (User)this.userService.findOneEagerly(userId);
        for (final Long id : playlists) {
            user.getPlaylists().add((Playlist)this.playlistService.findOne(id));
        }
        this.userService.save(user);
        return "";
    }
}
