// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller;

import java.util.Set;
import com.nevreme.rolling.model.Role;
import java.util.HashSet;
import org.springframework.validation.BindingResult;
import javax.validation.Valid;
import com.nevreme.rolling.model.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.UserService;
import org.springframework.stereotype.Controller;

@Controller
public class LoginController
{
    @Autowired
    private UserService userService;
    @Autowired
    AuthenticationTrustResolver authenticationTrustResolver;
    
    @RequestMapping(value = { "/login" }, method = { RequestMethod.GET })
    public String login() {
        final String viewName = this.isCurrentAuthenticationAnonymous() ? "login" : ("redirect:" + System.getProperty("APP_ROOT") + "/");
        return viewName;
    }
    
    @RequestMapping(value = { "/access-denied" }, method = { RequestMethod.GET })
    public ModelAndView accessDenied() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("access-denied");
        return modelAndView;
    }
    
    @RequestMapping(value = { "/contact" }, method = { RequestMethod.GET })
    public String contact() {
        return "contact";
    }
    
    @RequestMapping(value = { "/sport/scrolling" }, method = { RequestMethod.GET })
    public ModelAndView scrolling() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.setViewName("admin/scrolling");
        modelAndView.addObject("playlist_id", (Object)"1256");
        modelAndView.addObject("m_title", (Object)"ScRolling Sport");
        modelAndView.addObject("m_url", (Object)"http://radiorolling.com/sport/scrolling");
        modelAndView.addObject("m_desc", (Object)"Let it ScRoll");
        modelAndView.addObject("m_image", (Object)"http://radiorolling.com/res/images/20183231327/sport.jpg");
        return modelAndView;
    }
    
    @RequestMapping(value = { "/movies/scrolling" }, method = { RequestMethod.GET })
    public ModelAndView moviesScrolling() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.setViewName("admin/scrolling");
        modelAndView.addObject("playlist_id", (Object)"915");
        modelAndView.addObject("m_title", (Object)"ScRolling Movies");
        modelAndView.addObject("m_url", (Object)"http://radiorolling.com/movies/scrolling");
        modelAndView.addObject("m_desc", (Object)"Let it ScRoll");
        modelAndView.addObject("m_image", (Object)"http://radiorolling.com/res/images/2018431224/Movie.jpg");
        return modelAndView;
    }
    
    @RequestMapping(value = { "/", "" }, method = { RequestMethod.GET })
    public String hom() {
        return "redirect:" + System.getProperty("APP_ROOT") + "/home/";
    }
    
    private boolean isCurrentAuthenticationAnonymous() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication == null || this.authenticationTrustResolver.isAnonymous(authentication);
    }
    
    @RequestMapping(value = { "/vest" }, method = { RequestMethod.GET })
    public ModelAndView news() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.setViewName("post_view");
        return modelAndView;
    }
    
    @RequestMapping(value = { "/registration" }, method = { RequestMethod.GET })
    public ModelAndView registration() {
        final ModelAndView modelAndView = new ModelAndView();
        final User user = new User();
        modelAndView.addObject("user", (Object)user);
        modelAndView.setViewName("registration");
        return modelAndView;
    }
    
    @RequestMapping(value = { "/registration" }, method = { RequestMethod.POST })
    public ModelAndView createNewUser(@Valid final User user, final BindingResult bindingResult) {
        final ModelAndView modelAndView = new ModelAndView();
        final User userExists = this.userService.findUserByEmail(user.getUsername());
        if (userExists != null) {
            bindingResult.rejectValue("username", "error.user", "There is already a user registered with the email provided");
        }
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("registration");
        }
        else {
            final Set<Role> roles = new HashSet<Role>();
            final Role role = new Role();
            role.setRole("ADMIN");
            roles.add(role);
            user.setRoles(roles);
            this.userService.save(user);
            modelAndView.addObject("successMessage", (Object)"User has been registered successfully");
            modelAndView.addObject("user", (Object)new User());
            modelAndView.setViewName("registration");
        }
        return modelAndView;
    }
    
    @RequestMapping(value = { "/admin/home" }, method = { RequestMethod.GET })
    public String home() {
        System.out.println("******************************************");
        System.out.println(System.getProperty("APP_ROOT"));
        System.out.println("******************************************");
        return "redirect:" + System.getProperty("APP_ROOT") + "/admin/setup";
    }
}
