// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.AbstractService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.nevreme.rolling.dto.HtmlTemplateDto;
import com.nevreme.rolling.model.HtmlTemplate;

@Controller
@RequestMapping({ "/public/api/template/" })
public class HtmlTemplateRestController extends AbstractRestController<HtmlTemplate, HtmlTemplateDto, Long>
{
    @Autowired
    public HtmlTemplateRestController(final AbstractService<HtmlTemplate, Long> repo, final HtmlTemplateDto dto) {
        super((AbstractService)repo, dto);
    }
}
