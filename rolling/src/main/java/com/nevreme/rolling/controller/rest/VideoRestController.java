// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller.rest;

import com.nevreme.rolling.dto.PlaylistDto;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.CrossOrigin;
import java.io.IOException;
import java.util.Base64;
import java.io.ByteArrayOutputStream;
import java.io.BufferedInputStream;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Date;
import java.sql.Timestamp;
import java.io.Serializable;
import com.nevreme.rolling.model.Playlist;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.nevreme.rolling.service.AbstractService;
import com.nevreme.rolling.utils.PlaylistUtils;
import com.nevreme.rolling.service.PlaylistService;
import com.nevreme.rolling.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.dao.mapping.MapperConfig;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.nevreme.rolling.dto.VideoDto;
import com.nevreme.rolling.model.Video;

@Controller
@RequestMapping({ "/public/api/video/" })
public class VideoRestController extends AbstractRestController<Video, VideoDto, Long>
{
    @Autowired
    MapperConfig mapper;
    @Autowired
    VideoService videoService;
    @Autowired
    PlaylistService playlistService;
    @Autowired
    PlaylistUtils playlistUtils;
    
    @Autowired
    public VideoRestController(final AbstractService<Video, Long> repo, final VideoDto dto) {
        super((AbstractService)repo, dto);
    }
    
    @RequestMapping({ "/playing", "/playing/" })
    @ResponseBody
    public ResponseEntity<Video> findCurrent() {
        return (ResponseEntity<Video>)new ResponseEntity((Object)this.videoService.findVideoByState(0), HttpStatus.OK);
    }
    
    @RequestMapping({ "/newVideo", "/newVideo/" })
    @ResponseBody
    public synchronized String insertVideo(@RequestBody final VideoDto videoDto, @RequestParam final Long pl) {
        final Playlist playlist = (Playlist)this.playlistService.findOne(pl);
        final Video video = new Video();
        video.setDescription(videoDto.getDescription());
        video.setPlaylist(playlist);
        video.setDuration(videoDto.getDuration());
        video.setStarted(null);
        video.setState(1);
        video.setOffset(videoDto.getOffset());
        video.setId(null);
        video.setYtId(videoDto.getYtId());
        video.setQuote(videoDto.getQuote());
        if (playlist.getVideos().size() == 0) {
            video.setIndex_num(1);
            video.setState(0);
            this.videoService.save(video);
            return "{\"status\":\"ok\"}";
        }
        this.videoService.insertNewVideo(video);
        return "{\"status\":\"ok\"}";
    }
    
    @RequestMapping({ "/startPlaylist", "/startPlaylist/" })
    @ResponseBody
    public synchronized String startPlaylist(@RequestParam final Long pl) {
        final Video video = this.videoService.findVideoByStateForPlaylist(0, pl);
        final Playlist playlist = (Playlist)this.playlistService.findOne(pl);
        final long time = 0L;
        if (!playlist.isStart()) {
            playlist.setStart(true);
            this.playlistService.save(playlist);
            this.playlistUtils.executeJob(1000L * (video.getDuration() - time), pl);
            video.setStarted(new Timestamp(new Date().getTime()));
            this.videoService.save(video);
        }
        return "{\"status\":\"ok\"}";
    }
    
    @RequestMapping({ "/startPlaylists", "/startPlaylists/" })
    @ResponseBody
    public synchronized String startPlaylist() {
        final List<Playlist> playlists = (List<Playlist>)this.playlistService.getPLaylstByType(1);
        for (final Playlist playlist : playlists) {
            if (!playlist.isStart()) {
                playlist.setStart(true);
                this.playlistService.save(playlist);
                final Video video = this.videoService.findVideoByStateForPlaylist(0, playlist.getId());
                this.playlistUtils.executeJob((long)(1000 * video.getDuration()), playlist.getId());
                video.setStarted(new Timestamp(new Date().getTime()));
                this.videoService.save(video);
            }
        }
        return "{\"status\":\"ok\"}";
    }
    
    @CrossOrigin({ "*" })
    @Cacheable({ "sitecache" })
    @RequestMapping({ "/getImageFromUrl", "/getImageFromUrl/" })
    @ResponseBody
    public synchronized String getImageFromUrl(@RequestParam final String imageUrl) throws IOException {
        final URL url = new URL(imageUrl);
        final BufferedInputStream bis = new BufferedInputStream(url.openConnection().getInputStream());
        final byte[] buffer = new byte[1024];
        int read = 0;
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        while ((read = bis.read(buffer, 0, buffer.length)) != -1) {
            baos.write(buffer, 0, read);
        }
        baos.flush();
        final StringBuilder sb = new StringBuilder();
        sb.append("data:image/png;base64,");
        sb.append(new String(Base64.getEncoder().encode(baos.toByteArray())));
        return sb.toString();
    }
    
    @RequestMapping({ "/deleteVideo", "/deleteVideo/" })
    @ResponseBody
    public synchronized ResponseEntity<Set<Video>> deleteVideo(@RequestBody final VideoDto videoDto, @RequestParam final Long pl) {
        final Playlist plist = (Playlist)this.playlistService.findOne(pl);
        for (final Video v : plist.getVideos()) {
            if (v.getId() == (long)videoDto.getId()) {
                System.out.println("******************************************************");
                System.out.println("***********************DELETE*************************");
                System.out.println("******************************************************");
                plist.getVideos().remove(v);
                break;
            }
        }
        this.playlistService.save(plist);
        this.videoService.updateAllHigherThan(videoDto.getIndex_num(), pl);
        return (ResponseEntity<Set<Video>>)new ResponseEntity((Object)new HashSet(), HttpStatus.OK);
    }
    
    @CrossOrigin({ "*" })
    @RequestMapping({ "/getRecommendedVideo" })
    @ResponseBody
    public synchronized ResponseEntity<VideoDto> getRecommendedVideo(@RequestParam final int type) {
        final Video video = this.videoService.getRecommendedVideo(type);
        final VideoDto dto = (VideoDto)this.mapper.getMapper().map((Object)video, (Class)VideoDto.class);
        return (ResponseEntity<VideoDto>)new ResponseEntity((Object)dto, HttpStatus.OK);
    }
    
    @RequestMapping({ "/updatePlaylist", "/updatePlaylist/" })
    @ResponseBody
    public synchronized ResponseEntity<VideoDto[]> updateVideos(@RequestBody final VideoDto[] videosDto, @RequestParam final Long pl) {
        final List<Video> vids = new ArrayList<Video>();
        final Playlist p = (Playlist)this.playlistService.findOne(pl);
        for (final VideoDto vdto : videosDto) {
            final Video vid = this.videoService.findOne(vdto.getId());
            vid.setIndex_num(vdto.getIndex_num());
            vid.setPlaylist(p);
            vids.add(vid);
        }
        this.videoService.updateList((List)vids);
        return (ResponseEntity<VideoDto[]>)new ResponseEntity((Object)videosDto, HttpStatus.OK);
    }
    
    @CrossOrigin
    @RequestMapping({ "/addNewPlaylist", "/addNewPlaylist/" })
    @ResponseBody
    public synchronized ResponseEntity<PlaylistDto> addPlaylist(@RequestBody final PlaylistDto playlistDto) {
        final Playlist playlist = new Playlist();
        playlist.setImage(playlistDto.getImage());
        playlist.setName(playlistDto.getName());
        playlist.setStart(playlistDto.isStart());
        this.playlistService.save(playlist);
        return (ResponseEntity<PlaylistDto>)new ResponseEntity((Object)playlistDto, HttpStatus.OK);
    }
}
