// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.nevreme.rolling.dao.mapping.MapperConfig;
import com.nevreme.rolling.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.PlaylistService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@Controller
@RequestMapping({ "/admin" })
public class AdminController
{
    @Autowired
    PlaylistService playlistService;
    @Autowired
    UserService userService;
    @Autowired
    MapperConfig mapper;
    
    @RequestMapping(value = { "/playlistEdit", "/playlistEdit/" }, method = { RequestMethod.GET })
    public ModelAndView addPost() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("admin/playlist");
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        return modelAndView;
    }
    
    @RequestMapping(value = { "/setup", "/setup/" }, method = { RequestMethod.GET })
    public ModelAndView setup() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.setViewName("admin/playlists");
        return modelAndView;
    }
    
    @RequestMapping(value = { "/setupp" }, method = { RequestMethod.GET })
    @PreAuthorize("hasPermission(#id, 'id', 'read')")
    public ModelAndView setupid(@RequestParam final Long id) {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("playlist_id", (Object)id);
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.setViewName("admin/playlist");
        return modelAndView;
    }
    
    @RequestMapping(value = { "/playlistAdd/", "/playlistAdd" }, method = { RequestMethod.GET })
    public ModelAndView playlistAdd() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("admin/playlistAdd");
        return modelAndView;
    }
    
    @RequestMapping(value = { "/postAdd/", "/postAdd" }, method = { RequestMethod.GET })
    public ModelAndView postAdd() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.setViewName("admin/post");
        return modelAndView;
    }
    
    @RequestMapping(value = { "/recommendAdd/", "/recommendAdd" }, method = { RequestMethod.GET })
    public ModelAndView recommendAdd() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.setViewName("admin/postR");
        return modelAndView;
    }
}
