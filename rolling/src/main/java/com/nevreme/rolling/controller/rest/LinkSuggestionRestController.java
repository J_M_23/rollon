// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller.rest;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import com.nevreme.rolling.service.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.LinkSuggestionService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.nevreme.rolling.dto.LinkSuggestionDto;
import com.nevreme.rolling.model.LinkSuggestion;

@Controller
@RequestMapping({ "/public/api/suggestions/" })
public class LinkSuggestionRestController extends AbstractRestController<LinkSuggestion, LinkSuggestionDto, Long>
{
    @Autowired
    LinkSuggestionService linkService;
    
    @Autowired
    public LinkSuggestionRestController(final AbstractService<LinkSuggestion, Long> repo, final LinkSuggestionDto dto) {
        super((AbstractService)repo, dto);
    }
    
    @RequestMapping({ "/newLink", "/newLink/" })
    @ResponseBody
    public synchronized String insertCard(@RequestBody final LinkSuggestionDto linkDto) {
        final LinkSuggestion link = new LinkSuggestion();
        link.setLink(linkDto.getLink());
        link.setLinkType(linkDto.getLinkType());
        this.linkService.save(link);
        return "{\"status\":\"ok\"}";
    }
}
