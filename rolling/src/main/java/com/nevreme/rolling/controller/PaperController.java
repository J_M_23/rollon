// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller;

import org.springframework.web.bind.annotation.RequestMethod;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.nevreme.rolling.model.Video;
import com.nevreme.rolling.dto.VideoDto;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestParam;
import com.nevreme.rolling.dao.mapping.MapperConfig;
import com.nevreme.rolling.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.PlaylistService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@Controller
@RequestMapping({ "/paper" })
public class PaperController
{
    @Autowired
    PlaylistService playlistService;
    @Autowired
    VideoService videoService;
    @Autowired
    MapperConfig mapper;
    
    @RequestMapping(value = { "/", "" }, method = { RequestMethod.GET })
    public ModelAndView getPaper(@RequestParam final Long id, final HttpServletRequest request) throws JsonProcessingException {
        final ModelAndView modelAndView = new ModelAndView();
        final Video video = this.videoService.findOne(id);
        modelAndView.addObject("m_title", (Object)"Rolling Papiri\u0107i");
        modelAndView.addObject("m_url", (Object)("http://radiorolling.com/paper?id=" + id));
        modelAndView.addObject("m_desc", (Object)"Odlomci iz pesama, citati, izjave...");
        modelAndView.addObject("m_image", (Object)video.getYtId());
        modelAndView.addObject("paper", this.mapper.getMapper().map((Object)video, (Class)VideoDto.class));
        modelAndView.addObject("id", (Object)video.getId());
        modelAndView.setViewName("admin/paper");
        return modelAndView;
    }
}
