// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller.rest;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CrossOrigin;
import java.util.Iterator;
import org.springframework.http.HttpStatus;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import com.nevreme.rolling.service.AbstractService;
import com.nevreme.rolling.service.PlaylistService;
import com.nevreme.rolling.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.dao.mapping.MapperConfig;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.nevreme.rolling.dto.VideoDto;
import com.nevreme.rolling.model.Video;

@Controller
@RequestMapping({ "/public/api/paper/" })
public class PaperRestController extends AbstractRestController<Video, VideoDto, Long>
{
    @Autowired
    MapperConfig mapper;
    @Autowired
    VideoService videoService;
    @Autowired
    PlaylistService playlistService;
    
    @Autowired
    public PaperRestController(final AbstractService<Video, Long> repo, final VideoDto dto) {
        super((AbstractService)repo, dto);
    }
    
    @RequestMapping({ "/range", "/range/" })
    @CrossOrigin({ "*" })
    @ResponseBody
    public ResponseEntity<List<VideoDto>> findInRange(@RequestParam(name = "p") final Long p, @RequestParam(name = "start") final int start, @RequestParam(name = "max") final int max) {
        final List<VideoDto> dtos = new ArrayList<VideoDto>();
        for (final Video v : this.videoService.findVideosForPlaylistByRange(p, start, max)) {
            dtos.add((VideoDto)this.mapper.getMapper().map((Object)v, (Class)VideoDto.class));
        }
        return (ResponseEntity<List<VideoDto>>)new ResponseEntity((Object)dtos, HttpStatus.OK);
    }
}
