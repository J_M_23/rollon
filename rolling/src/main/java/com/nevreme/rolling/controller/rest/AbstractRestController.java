// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller.rest;

import org.springframework.web.bind.annotation.PathVariable;
import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import java.util.Iterator;
import java.util.List;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.dao.mapping.MapperConfig;
import com.nevreme.rolling.service.AbstractService;
import org.slf4j.Logger;
import java.io.Serializable;

public abstract class AbstractRestController<T, TDTO, ID extends Serializable>
{
    private Logger logger;
    private AbstractService<T, ID> repo;
    Class<?> clazz;
    @Autowired
    private MapperConfig mapper;
    
    public AbstractRestController(final AbstractService<T, ID> repo, final TDTO dto) {
        this.logger = LoggerFactory.getLogger((Class)com.nevreme.rolling.controller.rest.AbstractRestController.class);
        this.repo = repo;
        this.clazz = dto.getClass();
    }
    
    @CrossOrigin({ "*" })
    @RequestMapping(method = { RequestMethod.GET })
    @ResponseBody
    public List<T> listAll() {
        final List<T> all = (List<T>)this.repo.findAll();
        int i = 0;
        for (T t : all) {
            all.set(i, t = (T)this.mapper.getMapper().map((Object)t, this.clazz));
            ++i;
        }
        return all;
    }
    
    @CrossOrigin({ "*" })
    @RequestMapping(value = { "/eager" }, method = { RequestMethod.GET })
    @ResponseBody
    public List<T> listAllEagerly() {
        final List<T> all = (List<T>)this.repo.findAllEagerly();
        int i = 0;
        for (T t : all) {
            all.set(i, t = (T)this.mapper.getMapper().map((Object)t, this.clazz));
            ++i;
        }
        return all;
    }
    
    @CrossOrigin
    @RequestMapping(method = { RequestMethod.POST }, produces = { "application/json" }, consumes = { "application/json" })
    @ResponseBody
    public Map<String, Object> create(@RequestBody final String json) {
        this.repo.saveAsSql(json);
        final Map<String, Object> m = new HashMap<String, Object>();
        m.put("success", true);
        return m;
    }
    
    @CrossOrigin({ "*" })
    @RequestMapping(value = { "/{id}" }, method = { RequestMethod.GET })
    @ResponseBody
    public T get(@PathVariable final ID id) {
        T obj = (T)this.repo.findOne(id);
        obj = (T)this.mapper.getMapper().map((Object)obj, this.clazz);
        return obj;
    }
    
    @CrossOrigin({ "*" })
    @RequestMapping(value = { "/eager/{id}" }, method = { RequestMethod.GET })
    @ResponseBody
    public T getEager(@PathVariable final ID id) {
        T obj = (T)this.repo.findOneEagerly(id);
        obj = (T)this.mapper.getMapper().map((Object)obj, this.clazz);
        return obj;
    }
    
    @CrossOrigin({ "*" })
    @RequestMapping(value = { "/{id}" }, method = { RequestMethod.POST }, consumes = { "application/json" })
    @ResponseBody
    public Map<String, Object> update(@PathVariable final ID id, @RequestBody final String json) {
        this.logger.debug("update() of id#{} with body {}", (Object)id, (Object)json);
        this.logger.debug("T json is of type {}", (Object)json.getClass());
        this.repo.updateAsSql(json, id.toString());
        final Map<String, Object> m = new HashMap<String, Object>();
        m.put("success", true);
        m.put("id", id);
        return m;
    }
    
    @CrossOrigin({ "*" })
    @RequestMapping(value = { "/{id}" }, method = { RequestMethod.DELETE })
    @ResponseBody
    public Map<String, Object> delete(@PathVariable final ID id) {
        this.repo.delete(id);
        final Map<String, Object> m = new HashMap<String, Object>();
        m.put("success", true);
        return m;
    }
}
