// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.nevreme.rolling.utils.PlaylistUtils;
import com.nevreme.rolling.service.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.VideoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@Controller
@RequestMapping({ "/home" })
public class StartPageCotroller
{
    @Autowired
    VideoService videoService;
    @Autowired
    PlaylistService playlistService;
    @Autowired
    PlaylistUtils playlistUtils;
    private boolean started;
    
    public StartPageCotroller() {
        this.started = false;
    }
    
    @RequestMapping(value = { "/", "" }, method = { RequestMethod.GET })
    public ModelAndView addPost() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.setViewName("startPage");
        return modelAndView;
    }
    
    @RequestMapping(value = { "/sugg", "/sugg/" }, method = { RequestMethod.GET })
    public ModelAndView sugg() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("admin/linkSuggestions");
        return modelAndView;
    }
    
    @RequestMapping(value = { "/remote", "/remote/" }, method = { RequestMethod.GET })
    public ModelAndView remote() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("admin/remote");
        return modelAndView;
    }
}
