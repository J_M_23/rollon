// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller.rest;

import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.File;
import com.nevreme.rolling.utils.Constants;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CrossOrigin;
import java.util.Set;
import java.util.Iterator;
import java.util.List;
import java.io.Serializable;
import com.nevreme.rolling.model.Playlist;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import com.nevreme.rolling.model.Tag;
import com.nevreme.rolling.dto.TagDto;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import com.nevreme.rolling.service.AbstractService;
import com.nevreme.rolling.service.PlaylistService;
import com.nevreme.rolling.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.VideoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.nevreme.rolling.dto.VideoDto;
import com.nevreme.rolling.model.Video;

@Controller
@RequestMapping({ "/admin/api/recommend/" })
public class RecommendRestController extends AbstractRestController<Video, VideoDto, Long>
{
    @Autowired
    VideoService videoService;
    @Autowired
    TagService tagService;
    @Autowired
    PlaylistService playlistService;
    
    @Autowired
    public RecommendRestController(final AbstractService<Video, Long> repo, final VideoDto dto) {
        super((AbstractService)repo, dto);
    }
    
    @CrossOrigin(origins = { "*" })
    @RequestMapping({ "/add" })
    @ResponseBody
    public String get2PtsShotsForAPlayer(@RequestBody final VideoDto videoDto, @RequestParam final Long playlist_id) {
        final Video video = new Video();
        final List<Tag> allTags = (List<Tag>)this.tagService.findAll();
        for (final TagDto tagDto : videoDto.getTags()) {
            for (final Tag tag : allTags) {
                if (tag.getName().equals(tagDto.getName())) {
                    tagDto.setId(tag.getId());
                }
            }
        }
        final Set<Tag> postTags = new HashSet<Tag>();
        for (final TagDto tagDto2 : videoDto.getTags()) {
            final Tag newTag = new Tag();
            newTag.setId(tagDto2.getId());
            newTag.setName(tagDto2.getName());
            postTags.add(newTag);
        }
        video.setStarted(new Timestamp(new Date().getTime()));
        video.setDescription(videoDto.getDescription());
        video.setQuote(videoDto.getQuote());
        video.setYtId(videoDto.getYtId());
        video.setState(1);
        video.setTags(postTags);
        video.setPlaylist((Playlist)this.playlistService.findOne(playlist_id));
        this.videoService.insertNewVideo(video);
        return "";
    }
    
    @CrossOrigin(origins = { "*" })
    @RequestMapping({ "/saveImg" })
    @ResponseBody
    public String upload(@RequestParam("file") final MultipartFile img) throws IOException {
        return this.uploadFileHandler(img, img.getOriginalFilename());
    }
    
    public String uploadFileHandler(final MultipartFile file, final String name) {
        if (!file.isEmpty()) {
            try {
                final byte[] bytes = file.getBytes();
                final File dir = new File(Constants.IMAGE_PATH);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                final Timestamp date = new Timestamp(new Date().getTime());
                final String foldName = date.getYear() + 1900 + "" + (date.getMonth() + 1) + "" + date.getDate() + "" + date.getHours() + "" + date.getMinutes();
                final File dirDate = new File(Constants.IMAGE_PATH + File.separator + foldName);
                if (!dirDate.exists()) {
                    dirDate.mkdirs();
                }
                final File serverFile = new File(dirDate.getAbsolutePath() + File.separator + name);
                final BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();
                System.out.println("Server File Location=" + serverFile.getAbsolutePath());
                return "/res/images/" + foldName + "/" + name;
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
                return "You failed to upload " + name + " => " + e.getMessage();
            }
        }
        return "You failed to upload " + name + " because the file was empty.";
    }
}
