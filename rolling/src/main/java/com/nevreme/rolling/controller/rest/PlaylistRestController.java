// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller.rest;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import com.nevreme.rolling.interceptors.MyUserPrincipal;
import org.springframework.security.core.Authentication;
import java.io.Serializable;
import com.nevreme.rolling.dto.PlaylistInfoDto;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.Iterator;
import java.util.List;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.RequestParam;
import com.nevreme.rolling.service.AbstractService;
import com.nevreme.rolling.service.PlaylistService;
import com.nevreme.rolling.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.dao.mapping.MapperConfig;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.nevreme.rolling.dto.PlaylistDto;
import com.nevreme.rolling.model.Playlist;

@Controller
@RequestMapping({ "/public/api/playlist/" })
public class PlaylistRestController extends AbstractRestController<Playlist, PlaylistDto, Long>
{
    @Autowired
    MapperConfig mapper;
    @Autowired
    VideoService videoService;
    @Autowired
    PlaylistService playlistService;
    
    @Autowired
    public PlaylistRestController(final AbstractService<Playlist, Long> repo, final PlaylistDto dto) {
        super((AbstractService)repo, dto);
    }
    
    @CrossOrigin({ "*" })
    @RequestMapping({ "t", "t/" })
    @ResponseBody
    public synchronized String getPlaylist(@RequestParam final int type) throws JsonProcessingException {
        final List<PlaylistDto> plDto = new ArrayList<PlaylistDto>();
        final List<Playlist> pls = (List<Playlist>)this.playlistService.getPLaylstByType(type);
        for (final Playlist p : pls) {
            plDto.add((PlaylistDto)this.mapper.getMapper().map((Object)p, (Class)PlaylistDto.class));
        }
        return new ObjectMapper().writeValueAsString((Object)plDto);
    }
    
    @CrossOrigin({ "*" })
    @RequestMapping({ "lazy", "lazy/" })
    @ResponseBody
    public synchronized String getPlaylistLazy(@RequestParam final int type) throws JsonProcessingException {
        final List<PlaylistInfoDto> plDto = new ArrayList<PlaylistInfoDto>();
        final List<Playlist> pls = (List<Playlist>)this.playlistService.getPLaylstByType(type);
        for (final Playlist p : pls) {
            plDto.add((PlaylistInfoDto)this.mapper.getMapper().map((Object)p, (Class)PlaylistInfoDto.class));
        }
        return new ObjectMapper().writeValueAsString((Object)plDto);
    }
    
    @CrossOrigin({ "*" })
    @RequestMapping({ "lazyOne", "lazyOne/" })
    @ResponseBody
    public synchronized String getPlaylistByIdLazy(@RequestParam final long id) throws JsonProcessingException {
        PlaylistDto plDto = null;
        final Playlist pls = (Playlist)this.playlistService.findOne(id);
        plDto = (PlaylistDto)this.mapper.getMapper().map((Object)pls, (Class)PlaylistDto.class);
        return new ObjectMapper().writeValueAsString((Object)plDto);
    }
    
    @RequestMapping({ "user", "user/" })
    @ResponseBody
    public synchronized String getUserPlaylist(final Authentication authentication) throws JsonProcessingException {
        final List<PlaylistDto> plDto = new ArrayList<PlaylistDto>();
        final MyUserPrincipal principal = (MyUserPrincipal)authentication.getPrincipal();
        if (principal.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
            return new ObjectMapper().writeValueAsString((Object)this.listAllEagerly());
        }
        final List<Playlist> pls = principal.getUser().getPlaylists();
        for (final Playlist p : pls) {
            plDto.add((PlaylistDto)this.mapper.getMapper().map((Object)p, (Class)PlaylistDto.class));
        }
        return new ObjectMapper().writeValueAsString((Object)plDto);
    }
}
