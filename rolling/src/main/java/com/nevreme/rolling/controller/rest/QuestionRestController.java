// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller.rest;

import java.util.ArrayList;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;
import javax.servlet.http.Cookie;
import com.nevreme.rolling.model.Visitor;
import org.springframework.http.HttpStatus;
import com.nevreme.rolling.model.Vote;
import java.io.Serializable;
import com.nevreme.rolling.model.Answer;
import com.nevreme.rolling.dto.AnswerDto;
import org.springframework.http.ResponseEntity;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.bind.annotation.RequestBody;
import com.nevreme.rolling.service.AbstractService;
import com.nevreme.rolling.dao.mapping.MapperConfig;
import com.nevreme.rolling.service.VisitorService;
import com.nevreme.rolling.service.VoteService;
import com.nevreme.rolling.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.QuestionService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.nevreme.rolling.dto.QuestionDto;
import com.nevreme.rolling.model.Question;

@Controller
@RequestMapping({ "/public/api/question/" })
public class QuestionRestController extends AbstractRestController<Question, QuestionDto, Long>
{
    @Autowired
    private QuestionService questionService;
    @Autowired
    private AnswerService answerService;
    @Autowired
    private VoteService voteService;
    @Autowired
    private VisitorService visitorService;
    @Autowired
    MapperConfig mapper;
    
    @Autowired
    public QuestionRestController(final AbstractService<Question, Long> repo, final QuestionDto dto) {
        super((AbstractService)repo, dto);
    }
    
    @RequestMapping({ "add", "add/" })
    @ResponseBody
    public synchronized String add(@RequestBody final QuestionDto dto) throws JsonProcessingException {
        final Question active = this.questionService.findOneEagerlyActive();
        active.setActive(0);
        this.questionService.save(active);
        final Question q = new Question();
        q.setName(dto.getName());
        q.setImage(dto.getImage());
        q.setActive(1);
        this.questionService.save(q);
        return "Success";
    }
    
    @CrossOrigin({ "*" })
    @RequestMapping(value = { "like/", "like" }, method = { RequestMethod.POST })
    @ResponseBody
    public synchronized ResponseEntity<AnswerDto> like(@RequestParam final String id, @RequestParam final boolean type, final HttpServletRequest request) {
        final Answer answer = (Answer)this.answerService.findOneEagerly(Long.parseLong(id));
        final Vote vote = new Vote();
        Visitor visitor = null;
        for (final Cookie cookie : request.getCookies()) {
            if (cookie.getName().equals("visitorId")) {
                System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                System.out.println("%%%%%%%%%%%%%% " + cookie.getValue() + " %%%%%%%%%");
                System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                visitor = this.visitorService.findByVisitorId(cookie.getValue());
            }
        }
        if (this.voteService.findVoteByVisitorAndAnswer(visitor.getId(), Long.valueOf(Long.parseLong(id))) != null) {
            return (ResponseEntity<AnswerDto>)new ResponseEntity((Object)new AnswerDto(), HttpStatus.BAD_REQUEST);
        }
        if (type) {
            answer.setUpVotes(answer.getUpVotes() + 1);
        }
        else {
            answer.setDownVotes(answer.getDownVotes() + 1);
        }
        vote.setAnswer(answer);
        vote.setVote(type);
        vote.setVisitor(visitor);
        this.voteService.save(vote);
        answer.getVotes().add(vote);
        return (ResponseEntity<AnswerDto>)new ResponseEntity(this.mapper.getMapper().map((Object)answer, (Class)AnswerDto.class), HttpStatus.OK);
    }
    
    @CrossOrigin({ "*" })
    @RequestMapping({ "addAnswer", "addAnswer/" })
    @ResponseBody
    public ResponseEntity<List<AnswerDto>> add(@RequestParam final Long id, @RequestParam final String username, @RequestBody final AnswerDto dto, final HttpServletRequest request) throws JsonProcessingException {
        final Question q = (Question)this.questionService.findOneEagerly(id);
        final Answer answer = new Answer();
        answer.setDownVotes(0);
        answer.setUpVotes(0);
        answer.setQuestion(q);
        answer.setHtml(dto.getHtml());
        answer.setReplyAnswer((Answer)null);
        answer.setText(dto.getText());
        answer.setDate(new Timestamp(new Date().getTime()));
        Visitor visitor = null;
        for (final Cookie cookie : request.getCookies()) {
            if (cookie.getName().equals("visitorId")) {
                visitor = this.visitorService.findByVisitorId(cookie.getValue());
                if (visitor == null) {
                    visitor = new Visitor();
                    visitor.setVisitorId(cookie.getValue());
                    visitor.setUsername(username);
                    visitor = (Visitor)this.visitorService.save(visitor);
                    break;
                }
            }
        }
        visitor.setUsername(username);
        answer.setVisitor(visitor);
        q.getAnswers().add(answer);
        try {
            this.questionService.save(q);
        }
        catch (Exception e) {
            return (ResponseEntity<List<AnswerDto>>)new ResponseEntity((Object)new ArrayList(), HttpStatus.OK);
        }
        final List<AnswerDto> dtos = new ArrayList<AnswerDto>();
        q.getAnswers().forEach(i -> dtos.add((AnswerDto)this.mapper.getMapper().map((Object)i, (Class)AnswerDto.class)));
        return (ResponseEntity<List<AnswerDto>>)new ResponseEntity((Object)dtos, HttpStatus.OK);
    }
    
    @CrossOrigin({ "*" })
    @RequestMapping({ "active/ac", "active/ac/" })
    @ResponseBody
    public synchronized ResponseEntity<QuestionDto> active(@RequestParam final Long lidl) {
        final Question q = this.questionService.findOneEagerlyActive();
        return (ResponseEntity<QuestionDto>)new ResponseEntity(this.mapper.getMapper().map((Object)q, (Class)QuestionDto.class), HttpStatus.OK);
    }
}
