// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller.rest;

import org.springframework.http.HttpStatus;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import java.sql.Timestamp;
import java.util.Date;
import org.springframework.web.bind.annotation.RequestBody;
import com.nevreme.rolling.service.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.CardService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.nevreme.rolling.dto.CardDto;
import com.nevreme.rolling.model.Card;

@Controller
@RequestMapping({ "/public/api/card/" })
public class CardRestController extends AbstractRestController<Card, CardDto, Long>
{
    @Autowired
    CardService cardService;
    
    @Autowired
    public CardRestController(final AbstractService<Card, Long> repo, final CardDto dto) {
        super((AbstractService)repo, dto);
    }
    
    @RequestMapping({ "/newCard", "/newCard/" })
    @ResponseBody
    public synchronized String insertCard(@RequestBody final CardDto cardDto) {
        final Card card = new Card();
        card.setDescription(cardDto.getDescription());
        card.setTitle(cardDto.getTitle());
        card.setPlaylistName(cardDto.getPlaylistName());
        card.setImg(cardDto.getImg());
        card.setLink(cardDto.getLink());
        card.setDate_entered(new Timestamp(new Date().getTime()));
        this.cardService.save(card);
        return "{\"status\":\"ok\"}";
    }
    
    @RequestMapping({ "/range", "/range/" })
    @ResponseBody
    public ResponseEntity<List<Card>> findCurrent(@RequestParam(name = "start") final int start, @RequestParam(name = "max") final int max) {
        return (ResponseEntity<List<Card>>)new ResponseEntity((Object)this.cardService.findAllInRange(start, max), HttpStatus.OK);
    }
}
