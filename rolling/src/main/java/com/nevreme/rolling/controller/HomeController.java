// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller;

import java.util.List;
import java.util.Random;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.nevreme.rolling.model.Video;
import com.nevreme.rolling.dto.VideoDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import java.io.Serializable;
import com.nevreme.rolling.model.Playlist;
import org.springframework.web.servlet.ModelAndView;
import com.nevreme.rolling.utils.PlaylistUtils;
import com.nevreme.rolling.service.PlaylistService;
import com.nevreme.rolling.dao.mapping.MapperConfig;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.VideoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@Controller
@RequestMapping({ "/music" })
public class HomeController
{
    @Autowired
    VideoService videoService;
    @Autowired
    MapperConfig mapper;
    @Autowired
    PlaylistService playlistService;
    @Autowired
    PlaylistUtils playlistUtils;
    private boolean started;
    
    public HomeController() {
        this.started = false;
    }
    
    @RequestMapping(value = { "autoplay/", "autoplay" }, method = { RequestMethod.GET })
    public ModelAndView addPost() {
        final ModelAndView modelAndView = new ModelAndView();
        final Long playlist_id = this.playlistService.getPlaylistByName("Rolling");
        modelAndView.addObject("playlist_id", (Object)playlist_id);
        modelAndView.addObject("m_title", (Object)"Rolling Music (Rolling)");
        modelAndView.addObject("m_url", (Object)"http://radiorolling.com/music/autoplay/Rolling");
        modelAndView.addObject("m_desc", (Object)"Radio Mode");
        modelAndView.addObject("m_image", (Object)((Playlist)this.playlistService.findOne(playlist_id)).getImage());
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.addObject("ws", (Object)((System.getProperty("WS") == null) ? "" : System.getProperty("WS")));
        modelAndView.setViewName("admin/home");
        return modelAndView;
    }
    
    @RequestMapping(value = { "manualplay/", "manualplay" }, method = { RequestMethod.GET })
    public ModelAndView statical() {
        final ModelAndView modelAndView = new ModelAndView();
        final Long playlist_id = this.playlistService.getPlaylistByName("Rolling");
        modelAndView.addObject("playlist_id", (Object)playlist_id);
        modelAndView.addObject("vid_id", (Object)(-1));
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.addObject("ws", (Object)((System.getProperty("WS") == null) ? "" : System.getProperty("WS")));
        modelAndView.addObject("m_title", (Object)"Rolling Music (Rolling)");
        modelAndView.addObject("m_url", (Object)"http://radiorolling.com/music/manualplay/Rolling");
        modelAndView.addObject("m_desc", (Object)"Manual Mode");
        modelAndView.addObject("filter", (Object)"");
        modelAndView.addObject("m_image", (Object)((Playlist)this.playlistService.findOne(playlist_id)).getImage());
        modelAndView.setViewName("admin/staticalMusic");
        return modelAndView;
    }
    
    @RequestMapping(value = { "manualplay/{name}/", "manualplay/{name}" }, method = { RequestMethod.GET })
    public ModelAndView staticalName(@PathVariable final String name) {
        final ModelAndView modelAndView = new ModelAndView();
        final Long playlist_id = this.playlistService.getPlaylistByName(name);
        modelAndView.addObject("playlist_id", (Object)playlist_id);
        modelAndView.addObject("vid_id", (Object)(-1));
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.addObject("ws", (Object)((System.getProperty("WS") == null) ? "" : System.getProperty("WS")));
        modelAndView.addObject("m_title", (Object)("Rolling Music (" + name + ")"));
        modelAndView.addObject("m_url", (Object)("http://radiorolling.com/music/manualplay/" + name));
        modelAndView.addObject("m_desc", (Object)"Manual Mode");
        modelAndView.addObject("filter", (Object)"");
        modelAndView.addObject("m_image", (Object)((Playlist)this.playlistService.findOne(playlist_id)).getImage());
        modelAndView.setViewName("admin/staticalMusic");
        return modelAndView;
    }
    
    @RequestMapping(value = { "manualplay/vid" }, method = { RequestMethod.GET })
    public ModelAndView staticalVid(@RequestParam final String vid, @RequestParam final String plName, @RequestParam final String filter) throws JsonProcessingException {
        final ModelAndView modelAndView = new ModelAndView();
        final Long playlist_id = this.playlistService.getPlaylistByName(plName);
        final Video video = this.videoService.findVideoByYtId(vid, playlist_id);
        modelAndView.addObject("playlist_id", (Object)playlist_id);
        modelAndView.addObject("vid_id", (Object)vid);
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.addObject("ws", (Object)((System.getProperty("WS") == null) ? "" : System.getProperty("WS")));
        modelAndView.addObject("m_title", (Object)("Rolling Music (" + plName + ")"));
        modelAndView.addObject("m_url", (Object)("http://radiorolling.com/music/manualplay/vid?vid=" + vid + "&plName=" + plName+"&filter="));
        modelAndView.addObject("m_desc", (Object)video.getDescription());
        modelAndView.addObject("filter", (Object)filter);
        modelAndView.addObject("videoDto", (Object)new ObjectMapper().writeValueAsString(this.mapper.getMapper().map((Object)video, (Class)VideoDto.class)));
        modelAndView.addObject("m_image", (Object)("https://i.ytimg.com/vi/" + vid + "/hqdefault.jpg"));
        modelAndView.setViewName("admin/staticalMusic");
        return modelAndView;
    }
    
    @RequestMapping(value = { "autoplay/{name}/", "autoplay/{name}" }, method = { RequestMethod.GET })
    public ModelAndView ids(@PathVariable final String name) {
        final ModelAndView modelAndView = new ModelAndView();
        final Long playlist_id = this.playlistService.getPlaylistByName(name);
        modelAndView.addObject("playlist_id", (Object)playlist_id);
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.addObject("ws", (Object)((System.getProperty("WS") == null) ? "" : System.getProperty("WS")));
        modelAndView.addObject("m_title", (Object)("Rolling Music (" + name + ")"));
        modelAndView.addObject("m_url", (Object)("http://radiorolling.com/music/autoplay/" + name));
        modelAndView.addObject("m_desc", (Object)"Radio Mode");
        modelAndView.addObject("m_image", (Object)((Playlist)this.playlistService.findOne(playlist_id)).getImage());
        modelAndView.setViewName("admin/home");
        return modelAndView;
    }
    
    @RequestMapping(value = { "random" }, method = { RequestMethod.GET })
    public ModelAndView random() throws JsonProcessingException {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.setViewName("admin/random");
        modelAndView.addObject("m_title", (Object)"Rolling Random Song");
        modelAndView.addObject("m_url", (Object)"http://radiorolling.com/music/random");
        modelAndView.addObject("m_desc", (Object)"Listen to random song from our playlists");
        modelAndView.addObject("m_image", (Object)"http://radiorolling.com/res/images/2018323936/dd.png");
        List<Video> videos;
        Random rand;
        int randomNum;
        for (videos = (List<Video>)this.videoService.findAll(), rand = new Random(), randomNum = rand.nextInt(videos.size()); videos.get(randomNum).getPlaylist().getPlaylist_type() != 1; randomNum = rand.nextInt(videos.size())) {}
        modelAndView.addObject("vidd", (Object)new ObjectMapper().writeValueAsString(this.mapper.getMapper().map((Object)videos.get(randomNum), (Class)VideoDto.class)));
        return modelAndView;
    }
    
    @RequestMapping(value = { "scrolling" }, method = { RequestMethod.GET })
    public ModelAndView scrolling() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("appRoot", (Object)System.getProperty("APP_ROOT"));
        modelAndView.setViewName("admin/scrolling");
        modelAndView.addObject("playlist_id", (Object)"1298");
        modelAndView.addObject("m_title", (Object)"ScRolling Music");
        modelAndView.addObject("m_url", (Object)"http://radiorolling.com/music/scrolling");
        modelAndView.addObject("m_desc", (Object)"Let it ScRoll");
        modelAndView.addObject("m_image", (Object)"http://radiorolling.com/res/images/20183252342/muzika.jpg");
        return modelAndView;
    }
}
