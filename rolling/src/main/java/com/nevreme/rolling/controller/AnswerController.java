// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller;

import org.springframework.web.bind.annotation.RequestMethod;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.nevreme.rolling.dto.AnswerDto;
import java.io.Serializable;
import com.nevreme.rolling.model.Answer;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestParam;
import com.nevreme.rolling.dao.mapping.MapperConfig;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.AnswerService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@Controller
@RequestMapping({ "/answer" })
public class AnswerController
{
    @Autowired
    AnswerService answerService;
    @Autowired
    MapperConfig mapper;
    
    @RequestMapping(value = { "/", "" }, method = { RequestMethod.GET })
    public ModelAndView getAnswer(@RequestParam final Long id, final HttpServletRequest request) throws JsonProcessingException {
        final ModelAndView modelAndView = new ModelAndView();
        final Answer answer = (Answer)this.answerService.findOneEagerly(id);
        modelAndView.addObject("m_title", (Object)("Rolling Pitanje: '" + answer.getQuestion().getName() + "'"));
        modelAndView.addObject("m_url", (Object)("http://radiorolling.com/answer?id=" + id));
        modelAndView.addObject("m_desc", (Object)(answer.getVisitor().getUsername() + ": \"" + answer.getHtml() + "\""));
        modelAndView.addObject("m_image", (Object)answer.getQuestion().getImage());
        modelAndView.addObject("answer", this.mapper.getMapper().map((Object)answer, (Class)AnswerDto.class));
        modelAndView.addObject("name", (Object)answer.getQuestion().getName());
        modelAndView.addObject("id", (Object)answer.getQuestion().getId());
        modelAndView.setViewName("admin/answer");
        return modelAndView;
    }
}
