// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.controller;

import org.springframework.web.bind.annotation.RequestMethod;
import com.fasterxml.jackson.core.JsonProcessingException;
import javax.servlet.http.Cookie;
import com.nevreme.rolling.dto.VisitorDto;
import com.nevreme.rolling.model.Visitor;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;
import com.nevreme.rolling.dto.QuestionDto;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestParam;
import com.nevreme.rolling.dao.mapping.MapperConfig;
import com.nevreme.rolling.service.VisitorService;
import com.nevreme.rolling.service.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.QuestionService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@Controller
@RequestMapping({ "/question" })
public class QuestionController
{
    @Autowired
    QuestionService questionService;
    @Autowired
    VoteService voteService;
    @Autowired
    VisitorService visitorService;
    @Autowired
    MapperConfig mapper;
    
    @RequestMapping(value = { "/", "" }, method = { RequestMethod.GET })
    public ModelAndView getQuestion(@RequestParam final Long id, final HttpServletRequest request) throws JsonProcessingException {
        final ModelAndView modelAndView = new ModelAndView();
        Visitor visitor = null;
        if (request != null && request.getCookies() != null) {
            for (final Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals("visitorId")) {
                    visitor = this.visitorService.findByVisitorId(cookie.getValue());
                }
            }
        }
        modelAndView.addObject("m_title", (Object)"Rolling Pitanja");
        modelAndView.addObject("m_url", (Object)("http://radiorolling.com/question?id=" + id));
        modelAndView.addObject("m_desc", (Object)"\u0160aljite nam odgovore na pitanja o raznoraznim temama");
        final QuestionDto dto = (QuestionDto)this.mapper.getMapper().map(this.questionService.findOneEagerly(id), (Class)QuestionDto.class);
        modelAndView.addObject("dto", (Object)dto);
        modelAndView.addObject("visitor", (Object)new ObjectMapper().writeValueAsString(this.mapper.getMapper().map((Object)((visitor == null) ? new Visitor() : visitor), (Class)VisitorDto.class)));
        modelAndView.setViewName("admin/question");
        return modelAndView;
    }
}
