// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.utils;

import java.io.File;

public class Constants
{
    public static final String APP_ROOT = "http://localhost:8081";
    public static final String IMAGE_PATH;
    public static final String IMAGES_CONFIG_LOCATION = "file:///var/lib/rolling/res/images/";
    
    static {
        IMAGE_PATH = File.separator + "var" + File.separator + "lib" + File.separator + "rolling" + File.separator + "res" + File.separator + "images";
    }
}
