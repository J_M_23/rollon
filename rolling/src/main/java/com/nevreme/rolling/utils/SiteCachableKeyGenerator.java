// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.utils;

import java.util.List;
import java.util.ArrayList;
import java.lang.reflect.Method;
import org.springframework.cache.interceptor.SimpleKeyGenerator;

public class SiteCachableKeyGenerator extends SimpleKeyGenerator
{
    public Object generate(final Object target, final Method method, final Object... params) {
        final List<Object> p = new ArrayList<Object>();
        p.add(method.getName());
        if (params != null) {
            for (final Object param : params) {
                p.add(param);
            }
        }
        return p;
    }
}
