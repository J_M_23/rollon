// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.configuration;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.stereotype.Component;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;

@Component
public class CustomContainer implements EmbeddedServletContainerCustomizer
{
    public void customize(final ConfigurableEmbeddedServletContainer container) {
        container.setPort(Integer.parseInt((System.getProperty("PORT") == null) ? "8081" : System.getProperty("PORT")));
    }
}
