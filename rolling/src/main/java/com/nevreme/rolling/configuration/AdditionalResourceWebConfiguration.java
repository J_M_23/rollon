// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.configuration;

import org.springframework.validation.Validator;
import org.springframework.validation.MessageCodesResolver;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import java.util.List;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AdditionalResourceWebConfiguration implements WebMvcConfigurer
{
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler(new String[] { "res/images/**" }).addResourceLocations(new String[] { "file:///var/lib/rolling/res/images/" });
    }
    
    public void addArgumentResolvers(final List<HandlerMethodArgumentResolver> arg0) {
    }
    
    public void addCorsMappings(final CorsRegistry arg0) {
    }
    
    public void addFormatters(final FormatterRegistry arg0) {
    }
    
    public void addInterceptors(final InterceptorRegistry arg0) {
    }
    
    public void addReturnValueHandlers(final List<HandlerMethodReturnValueHandler> arg0) {
    }
    
    public void addViewControllers(final ViewControllerRegistry arg0) {
    }
    
    public void configureAsyncSupport(final AsyncSupportConfigurer arg0) {
    }
    
    public void configureContentNegotiation(final ContentNegotiationConfigurer arg0) {
    }
    
    public void configureDefaultServletHandling(final DefaultServletHandlerConfigurer arg0) {
    }
    
    public void configureHandlerExceptionResolvers(final List<HandlerExceptionResolver> arg0) {
    }
    
    public void configureMessageConverters(final List<HttpMessageConverter<?>> arg0) {
    }
    
    public void configurePathMatch(final PathMatchConfigurer arg0) {
    }
    
    public void configureViewResolvers(final ViewResolverRegistry arg0) {
    }
    
    public void extendHandlerExceptionResolvers(final List<HandlerExceptionResolver> arg0) {
    }
    
    public void extendMessageConverters(final List<HttpMessageConverter<?>> arg0) {
    }
    
    public MessageCodesResolver getMessageCodesResolver() {
        return null;
    }
    
    public Validator getValidator() {
        return null;
    }
}
