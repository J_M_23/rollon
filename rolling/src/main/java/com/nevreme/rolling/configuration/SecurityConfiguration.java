// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.configuration;

import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.annotation.web.configurers.FormLoginConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Autowired;
import javax.sql.DataSource;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter
{
    @Autowired
    @Qualifier("primaryDatasource")
    private DataSource primaryDatasource;
    @Autowired
    @Qualifier("secondDatasource")
    private DataSource secondDatasource;
    @Value("${spring.queries.users-query}")
    private String usersQuery;
    @Value("${spring.queries.roles-query}")
    private String rolesQuery;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    @Qualifier("userDetailsService")
    UserDetailsService userDetailsService;
    
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        final DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(this.userDetailsService);
        authProvider.setPasswordEncoder((Object)this.bCryptPasswordEncoder);
        return authProvider;
    }
    
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider((AuthenticationProvider)this.authenticationProvider());
        auth.jdbcAuthentication().usersByUsernameQuery(this.usersQuery).authoritiesByUsernameQuery(this.rolesQuery).dataSource(this.secondDatasource);
    }
    
    protected void configure(final HttpSecurity http) throws Exception {
        if (System.getProperty("APP_ROOT") == null) {
            System.out.println("****SETUP NOT OK****");
            System.setProperty("APP_ROOT", "http://localhost:8081");
        }
        System.out.println("************APPROOT**************" + System.getProperty("APP_ROOT"));
        ((HttpSecurity)((HttpSecurity)((FormLoginConfigurer)((FormLoginConfigurer)((HttpSecurity)((HttpSecurity)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)http.authorizeRequests().antMatchers(new String[] { "/" })).permitAll().antMatchers(new String[] { "/p/**" })).permitAll().antMatchers(new String[] { "/ws/**" })).permitAll().antMatchers(new String[] { "/res/**" })).permitAll().antMatchers(new String[] { "/public/**" })).permitAll().antMatchers(new String[] { "/img**/**" })).permitAll().antMatchers(new String[] { "/home/**" })).permitAll().antMatchers(new String[] { "/music/**" })).permitAll().antMatchers(new String[] { "/contact/**" })).permitAll().antMatchers(new String[] { "/answer/**" })).permitAll().antMatchers(new String[] { "/paper/**" })).permitAll().antMatchers(new String[] { "/question/**" })).permitAll().antMatchers(new String[] { "/movies/**" })).permitAll().antMatchers(new String[] { "/sport/**" })).permitAll().antMatchers(new String[] { "/culture/**" })).permitAll().antMatchers(new String[] { "/video/**" })).permitAll().antMatchers(new String[] { "/recommend/**" })).permitAll().antMatchers(new String[] { "/text/**" })).permitAll().antMatchers(new String[] { "/login" })).permitAll().antMatchers(new String[] { "/editor/**" })).hasAnyRole(new String[] { "ADMIN" }).anyRequest()).authenticated().antMatchers(new String[] { "/admin/**" })).hasAnyRole(new String[] { "ADMIN" }).anyRequest()).authenticated().and()).csrf().disable()).formLogin().loginPage("/login").failureUrl("/login?error=true")).defaultSuccessUrl(String.valueOf(System.getProperty("APP_ROOT")) + "/admin/setup")).usernameParameter("username").passwordParameter("password").and()).logout().logoutRequestMatcher((RequestMatcher)new AntPathRequestMatcher("/logout")).logoutSuccessUrl(System.getProperty("APP_ROOT")).and()).exceptionHandling().accessDeniedPage("/access-denied");
    }
    
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring().antMatchers(new String[] { "/resources/**", "/static/**", "/css/**", "/js/**", "/images/**", "/theme/**" });
    }
}
