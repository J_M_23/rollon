// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.configuration;

import org.springframework.cache.interceptor.SimpleCacheErrorHandler;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.SimpleCacheResolver;
import org.springframework.cache.interceptor.CacheResolver;
import com.nevreme.rolling.utils.SiteCachableKeyGenerator;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import net.sf.ehcache.CacheManager;
import org.springframework.instrument.classloading.LoadTimeWeaver;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import javax.persistence.SharedCacheMode;
import org.springframework.orm.jpa.persistenceunit.DefaultPersistenceUnitManager;
import org.hibernate.dialect.PostgreSQL9Dialect;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.hibernate.jpa.HibernatePersistenceProvider;
import java.util.Properties;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.persistenceunit.PersistenceUnitManager;
import org.springframework.orm.jpa.JpaTransactionManager;
import javax.persistence.EntityManagerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import javax.sql.DataSource;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.context.annotation.Configuration;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableAsync
@EnableCaching
public class WebMvcConfig extends WebMvcConfigurerAdapter implements CachingConfigurer
{
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }
    
    public void addInterceptors(final InterceptorRegistry registry) {
        registry.addInterceptor((HandlerInterceptor)new CookieInterceptor());
    }
    
    @Bean
    public AuthenticationTrustResolver getAuthenticationTrustResolver() {
        return (AuthenticationTrustResolver)new AuthenticationTrustResolverImpl();
    }
    
    @Bean(name = { "primaryDatasource" })
    @ConfigurationProperties(prefix = "spring.primaryDatasource")
    public DataSource primaryDatasource() {
        return DataSourceBuilder.create().build();
    }
    
    @Bean(name = { "secondDatasource" })
    @Primary
    @ConfigurationProperties(prefix = "spring.secondDatasource")
    public DataSource secondDatasource() {
        return DataSourceBuilder.create().build();
    }
    
    @Bean
    JpaTransactionManager transactionManager(final EntityManagerFactory factory) throws Exception {
        return new JpaTransactionManager(factory);
    }
    
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(final PersistenceUnitManager persistenceUnitManager, final JpaVendorAdapter jpaVendorAdapter, final DataSource secondDatasource) {
        final LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        final Properties properties = new Properties();
        properties.put("hibernate.listeners.envers.autoRegister", "false");
        properties.put("hibernate.format_sql", "false");
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.default_batch_fetch_size", "9");
        properties.put("hibernate.max_fetch_depth", "3");
        properties.put("cache.use_second_level_cache", "true");
        properties.put("cache.use_query_cache", "true");
        properties.put("hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.EhCacheRegionFactory");
        properties.put("hibernate.id.new_generator_mappings", "true");
        factory.setJpaProperties(properties);
        factory.setJpaVendorAdapter(jpaVendorAdapter);
        factory.setPersistenceProviderClass((Class)HibernatePersistenceProvider.class);
        factory.setPersistenceUnitManager(persistenceUnitManager);
        factory.setDataSource(secondDatasource);
        return factory;
    }
    
    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        final HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setDatabase(Database.POSTGRESQL);
        adapter.setDatabasePlatform(PostgreSQL9Dialect.class.getName());
        adapter.setShowSql(true);
        adapter.setGenerateDdl(true);
        return (JpaVendorAdapter)adapter;
    }
    
    @Bean
    PersistenceUnitManager persistenceUnitManager(final DataSource secondDatasource) {
        final DefaultPersistenceUnitManager persistenceUnitManager = new DefaultPersistenceUnitManager();
        persistenceUnitManager.setDefaultPersistenceUnitName("rolling");
        persistenceUnitManager.setDefaultDataSource(secondDatasource);
        persistenceUnitManager.setPackagesToScan(new String[] { "com.nevreme.rolling.*" });
        persistenceUnitManager.setSharedCacheMode(SharedCacheMode.DISABLE_SELECTIVE);
        persistenceUnitManager.setLoadTimeWeaver((LoadTimeWeaver)new InstrumentationLoadTimeWeaver());
        return (PersistenceUnitManager)persistenceUnitManager;
    }
    
    @Bean
    public CacheManager ehCacheManager() {
        final EhCacheManagerFactoryBean cmfb = new EhCacheManagerFactoryBean();
        cmfb.setConfigLocation((Resource)new ClassPathResource("ehcache.xml"));
        cmfb.setShared(true);
        return cmfb.getObject();
    }
    
    @Bean
    public org.springframework.cache.CacheManager cacheManager() {
        return (org.springframework.cache.CacheManager)new EhCacheCacheManager(this.ehCacheManager());
    }
    
    @Bean
    public KeyGenerator keyGenerator() {
        return (KeyGenerator)new SiteCachableKeyGenerator();
    }
    
    @Bean
    public CacheResolver cacheResolver() {
        final SimpleCacheResolver cr = new SimpleCacheResolver(this.cacheManager());
        return (CacheResolver)cr;
    }
    
    @Bean
    public CacheErrorHandler errorHandler() {
        return (CacheErrorHandler)new SimpleCacheErrorHandler();
    }
}
