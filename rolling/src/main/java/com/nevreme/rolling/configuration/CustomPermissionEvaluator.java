// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.configuration;

import com.nevreme.rolling.model.Playlist;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.List;
import java.io.Serializable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import com.nevreme.rolling.interceptors.MyUserPrincipal;
import org.springframework.security.core.Authentication;
import org.springframework.security.access.PermissionEvaluator;

public class CustomPermissionEvaluator implements PermissionEvaluator
{
    public boolean hasPermission(final Authentication authentication, final Object targetDomainObject, final Object permission) {
        if (authentication == null) {
            return false;
        }
        final MyUserPrincipal principal = (MyUserPrincipal)authentication.getPrincipal();
        if (principal.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
            return true;
        }
        final String targetType = targetDomainObject.getClass().getSimpleName().toUpperCase();
        return this.hasPermission(authentication, targetType, permission.toString().toUpperCase());
    }
    
    public boolean hasPermission(final Authentication authentication, final Serializable targetId, final String targetType, final Object permission) {
        if (authentication == null) {
            return false;
        }
        final MyUserPrincipal principal = (MyUserPrincipal)authentication.getPrincipal();
        if (principal.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
            return true;
        }
        final List<Playlist> playlists = principal.getUser().getPlaylists();
        return !playlists.stream().filter(playlist -> playlist.getId().equals(targetId)).collect((Collector<? super Object, ?, List<? super Object>>)Collectors.toList()).isEmpty();
    }
}
