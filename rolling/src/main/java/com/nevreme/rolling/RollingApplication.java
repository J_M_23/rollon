// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
@ComponentScan(basePackages = { "com.nevreme.rolling" })
public class RollingApplication extends SpringBootServletInitializer
{
    public static void main(final String[] args) {
        if (System.getProperty("APP_ROOT") == null) {
            System.setProperty("APP_ROOT", "http://localhost:8081");
        }
        SpringApplication.run((Object)RollingApplication.class, args);
    }
}
