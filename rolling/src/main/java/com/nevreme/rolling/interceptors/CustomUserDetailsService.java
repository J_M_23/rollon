// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.interceptors;

import java.util.Iterator;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import com.nevreme.rolling.model.Role;
import java.util.ArrayList;
import org.springframework.security.core.GrantedAuthority;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import com.nevreme.rolling.model.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.UserDetailsService;

@Service("userDetailsService")
public class CustomUserDetailsService implements UserDetailsService
{
    @Autowired
    private UserService adminService;
    
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(final String ssoId) throws UsernameNotFoundException {
        final User admin = this.adminService.findUserByEmail(ssoId);
        if (admin == null) {
            throw new UsernameNotFoundException("Username not found");
        }
        return (UserDetails)new MyUserPrincipal(admin);
    }
    
    private List<GrantedAuthority> getGrantedAuthorities(final User user) {
        final List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (final Role role : user.getRoles()) {
            authorities.add((GrantedAuthority)new SimpleGrantedAuthority(role.getRole()));
        }
        return authorities;
    }
}
