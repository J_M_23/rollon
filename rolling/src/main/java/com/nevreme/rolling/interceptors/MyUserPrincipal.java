// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.interceptors;

import java.util.Iterator;
import java.util.List;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import com.nevreme.rolling.model.Role;
import java.util.ArrayList;
import org.springframework.security.core.GrantedAuthority;
import java.util.Collection;
import com.nevreme.rolling.model.User;
import org.springframework.security.core.userdetails.UserDetails;

public class MyUserPrincipal implements UserDetails
{
    private static final long serialVersionUID = -5694665634575424841L;
    private User user;
    
    public MyUserPrincipal(final User user) {
        this.user = user;
    }
    
    public Long getId() {
        return this.user.getId();
    }
    
    public String getImage() {
        return this.user.getImage();
    }
    
    public Collection<? extends GrantedAuthority> getAuthorities() {
        final List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (final Role role : this.user.getRoles()) {
            authorities.add((GrantedAuthority)new SimpleGrantedAuthority(role.getRole()));
        }
        return authorities;
    }
    
    public String getPassword() {
        return this.user.getPassword();
    }
    
    public String getUsername() {
        return this.user.getUsername();
    }
    
    public User getUser() {
        return this.user;
    }
    
    public boolean isAccountNonExpired() {
        return true;
    }
    
    public boolean isAccountNonLocked() {
        return true;
    }
    
    public boolean isCredentialsNonExpired() {
        return true;
    }
    
    public boolean isEnabled() {
        return true;
    }
}
