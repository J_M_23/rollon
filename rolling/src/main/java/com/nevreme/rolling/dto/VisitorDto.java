// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dto;

import org.springframework.stereotype.Component;

@Component
public class VisitorDto
{
    private Long id;
    private String visitorId;
    private String username;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String getVisitorId() {
        return this.visitorId;
    }
    
    public void setVisitorId(final String visitorId) {
        this.visitorId = visitorId;
    }
    
    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(final String username) {
        this.username = username;
    }
}
