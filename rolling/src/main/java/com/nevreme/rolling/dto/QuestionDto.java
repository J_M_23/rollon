// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dto;

import java.util.Set;
import org.springframework.stereotype.Component;

@Component
public class QuestionDto
{
    private Long id;
    private String name;
    private String image;
    private int active;
    private Set<AnswerDto> answers;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getImage() {
        return this.image;
    }
    
    public void setImage(final String image) {
        this.image = image;
    }
    
    public Set<AnswerDto> getAnswers() {
        return this.answers;
    }
    
    public void setAnswers(final Set<AnswerDto> answers) {
        this.answers = answers;
    }
    
    public int getActive() {
        return this.active;
    }
    
    public void setActive(final int active) {
        this.active = active;
    }
}
