// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dto;

import org.springframework.stereotype.Component;

@Component
public class VideoDetailsDto
{
    private String plId;
    private String videoUrl;
    private String videoDescription;
    private String videoDuration;
    private String videoQuote;
    private int videoOffset;
    private String plName;
    
    public String getVideoUrl() {
        return this.videoUrl;
    }
    
    public void setVideoUrl(final String videoUrl) {
        this.videoUrl = videoUrl;
    }
    
    public String getVideoDescription() {
        return this.videoDescription;
    }
    
    public void setVideoDescription(final String videoDescription) {
        this.videoDescription = videoDescription;
    }
    
    public String getVideoDuration() {
        return this.videoDuration;
    }
    
    public void setVideoDuration(final String videoDuration) {
        this.videoDuration = videoDuration;
    }
    
    public String getVideoQuote() {
        return this.videoQuote;
    }
    
    public void setVideoQuote(final String videoQuote) {
        this.videoQuote = videoQuote;
    }
    
    public int getVideoOffset() {
        return this.videoOffset;
    }
    
    public void setVideoOffset(final int videoOffset) {
        this.videoOffset = videoOffset;
    }
    
    public String getPlName() {
        return this.plName;
    }
    
    public void setPlName(final String plName) {
        this.plName = plName;
    }
    
    public String getPlId() {
        return this.plId;
    }
    
    public void setPlId(final String plId) {
        this.plId = plId;
    }
}
