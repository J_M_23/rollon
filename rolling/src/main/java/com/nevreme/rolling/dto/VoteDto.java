// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dto;

import org.springframework.stereotype.Component;

@Component
public class VoteDto
{
    private Long id;
    private boolean vote;
    private Long visitorId;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public boolean isVote() {
        return this.vote;
    }
    
    public void setVote(final boolean vote) {
        this.vote = vote;
    }
    
    public Long getVisitorId() {
        return this.visitorId;
    }
    
    public void setVisitorId(final Long visitorId) {
        this.visitorId = visitorId;
    }
}
