// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dto;

import java.util.List;
import java.util.Set;
import org.springframework.stereotype.Component;

@Component
public class PlaylistDto
{
    private Long id;
    private String name;
    private Set<VideoDto> videos;
    private boolean start;
    private String image;
    private int playlist_type;
    private List<UserDto> userDto;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public Set<VideoDto> getVideos() {
        return this.videos;
    }
    
    public void setVideos(final Set<VideoDto> videos) {
        this.videos = videos;
    }
    
    public boolean isStart() {
        return this.start;
    }
    
    public void setStart(final boolean start) {
        this.start = start;
    }
    
    public String getImage() {
        return this.image;
    }
    
    public void setImage(final String image) {
        this.image = image;
    }
    
    public int getPlaylist_type() {
        return this.playlist_type;
    }
    
    public void setPlaylist_type(final int playlist_type) {
        this.playlist_type = playlist_type;
    }
    
    public List<UserDto> getUserDto() {
        return this.userDto;
    }
    
    public void setUserDto(final List<UserDto> userDto) {
        this.userDto = userDto;
    }
}
