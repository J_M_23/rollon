// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dto;

import java.sql.Timestamp;
import org.springframework.stereotype.Component;

@Component
public class CardDto
{
    private Long id;
    private String link;
    private String title;
    private String description;
    private String playlistName;
    private String img;
    private Timestamp date_entered;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String getLink() {
        return this.link;
    }
    
    public void setLink(final String link) {
        this.link = link;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(final String description) {
        this.description = description;
    }
    
    public String getPlaylistName() {
        return this.playlistName;
    }
    
    public void setPlaylistName(final String playlistName) {
        this.playlistName = playlistName;
    }
    
    public String getImg() {
        return this.img;
    }
    
    public void setImg(final String img) {
        this.img = img;
    }
    
    public Timestamp getDate_entered() {
        return this.date_entered;
    }
    
    public void setDate_entered(final Timestamp date_entered) {
        this.date_entered = date_entered;
    }
}
