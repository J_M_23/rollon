// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dto;

import java.util.Set;
import java.sql.Timestamp;
import org.springframework.stereotype.Component;

@Component
public class AnswerDto
{
    private Long id;
    private String text;
    private int upVotes;
    private int downVotes;
    private AnswerDto replyAnswer;
    private Timestamp date;
    private String html;
    private VisitorDto visitor;
    private Set<VoteDto> votes;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String getText() {
        return this.text;
    }
    
    public void setText(final String text) {
        this.text = text;
    }
    
    public int getUpVotes() {
        return this.upVotes;
    }
    
    public void setUpVotes(final int upVotes) {
        this.upVotes = upVotes;
    }
    
    public int getDownVotes() {
        return this.downVotes;
    }
    
    public void setDownVotes(final int downVotes) {
        this.downVotes = downVotes;
    }
    
    public AnswerDto getReplyAnswer() {
        return this.replyAnswer;
    }
    
    public void setReplyAnswer(final AnswerDto replyAnswer) {
        this.replyAnswer = replyAnswer;
    }
    
    public VisitorDto getVisitor() {
        return this.visitor;
    }
    
    public void setVisitor(final VisitorDto visitor) {
        this.visitor = visitor;
    }
    
    public Set<VoteDto> getVotes() {
        return this.votes;
    }
    
    public void setVotes(final Set<VoteDto> votes) {
        this.votes = votes;
    }
    
    public Timestamp getDate() {
        return this.date;
    }
    
    public void setDate(final Timestamp date) {
        this.date = date;
    }
    
    public String getHtml() {
        return this.html;
    }
    
    public void setHtml(final String html) {
        this.html = html;
    }
}
