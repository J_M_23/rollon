// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dto;

import org.springframework.stereotype.Component;

@Component
public class RequestMessageDto
{
    private int requestType;
    
    public int getRequestType() {
        return this.requestType;
    }
    
    public void setRequestType(final int requestType) {
        this.requestType = requestType;
    }
}
