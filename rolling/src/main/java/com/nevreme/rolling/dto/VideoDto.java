// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dto;

import java.util.Set;
import java.sql.Timestamp;
import org.springframework.stereotype.Component;

@Component
public class VideoDto
{
    private Long id;
    private String description;
    private Timestamp started;
    private Timestamp recommendDate;
    private String ytId;
    private int duration;
    private int index_num;
    private int state;
    private int offset;
    private String quote;
    private int dailyRecommend;
    private String plName;
    private Set<TagDto> tags;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(final String description) {
        this.description = description;
    }
    
    public Timestamp getStarted() {
        return this.started;
    }
    
    public void setStarted(final Timestamp started) {
        this.started = started;
    }
    
    public String getYtId() {
        return this.ytId;
    }
    
    public void setYtId(final String ytId) {
        this.ytId = ytId;
    }
    
    public int getDuration() {
        return this.duration;
    }
    
    public void setDuration(final int duration) {
        this.duration = duration;
    }
    
    public int getIndex_num() {
        return this.index_num;
    }
    
    public void setIndex_num(final int index_num) {
        this.index_num = index_num;
    }
    
    public int getState() {
        return this.state;
    }
    
    public void setState(final int state) {
        this.state = state;
    }
    
    public String getQuote() {
        return this.quote;
    }
    
    public void setQuote(final String quote) {
        this.quote = quote;
    }
    
    public int getOffset() {
        return this.offset;
    }
    
    public void setOffset(final int offset) {
        this.offset = offset;
    }
    
    public int getDailyRecommend() {
        return this.dailyRecommend;
    }
    
    public void setDailyRecommend(final int dailyRecommend) {
        this.dailyRecommend = dailyRecommend;
    }
    
    public String getPlName() {
        return this.plName;
    }
    
    public void setPlName(final String plName) {
        this.plName = plName;
    }
    
    public Set<TagDto> getTags() {
        return this.tags;
    }
    
    public void setTags(final Set<TagDto> tags) {
        this.tags = tags;
    }
    
    public Timestamp getRecommendDate() {
        return this.recommendDate;
    }
    
    public void setRecommendDate(final Timestamp recommendDate) {
        this.recommendDate = recommendDate;
    }
}
