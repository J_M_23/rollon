// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dto;

import org.springframework.stereotype.Component;

@Component
public class LinkSuggestionDto
{
    private Long id;
    private String link;
    private int linkType;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String getLink() {
        return this.link;
    }
    
    public void setLink(final String link) {
        this.link = link;
    }
    
    public int getLinkType() {
        return this.linkType;
    }
    
    public void setLinkType(final int linkType) {
        this.linkType = linkType;
    }
}
