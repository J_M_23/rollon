// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.dto;

import org.springframework.stereotype.Component;

@Component
public class TagDto
{
    private Long id;
    private String name;
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
}
