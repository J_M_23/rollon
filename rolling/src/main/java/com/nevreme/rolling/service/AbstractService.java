// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.service;

import java.util.List;
import com.nevreme.rolling.dao.AbstractDao;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;

@Transactional
public abstract class AbstractService<T, ID extends Serializable>
{
    private AbstractDao<T, ID> iDao;
    
    public AbstractService(final AbstractDao<T, ID> iDao) {
        this.iDao = iDao;
    }
    
    public T save(final T entity) {
        return (T)this.iDao.save(entity);
    }
    
    public T findOne(final ID primaryKey) {
        return (T)this.iDao.findOne(primaryKey);
    }
    
    public T findOneEagerly(final ID primaryKey) {
        return (T)this.iDao.findOneEagerly(primaryKey);
    }
    
    public List<T> findAll() {
        return (List<T>)this.iDao.findAll();
    }
    
    public List<T> findAllEagerly() {
        return (List<T>)this.iDao.findAllEagerly();
    }
    
    public Long count() {
        return this.iDao.count();
    }
    
    public void delete(final ID id) {
        this.iDao.delete(id);
    }
    
    public AbstractDao<T, ID> getiDao() {
        return (AbstractDao<T, ID>)this.iDao;
    }
    
    public void setiDao(final AbstractDao<T, ID> iDao) {
        this.iDao = iDao;
    }
    
    public void saveAsSql(final String sql) {
        this.iDao.saveWithSql(sql);
    }
    
    public void updateAsSql(final String sql, final String id) {
        this.iDao.updateWithSql(sql, id);
    }
}
