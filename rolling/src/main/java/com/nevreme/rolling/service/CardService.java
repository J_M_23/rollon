// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.service;

import java.util.List;
import com.nevreme.rolling.dao.AbstractDao;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.dao.CardDao;
import org.springframework.stereotype.Service;
import com.nevreme.rolling.model.Card;

@Service
public class CardService extends AbstractService<Card, Long>
{
    @Autowired
    CardDao dao;
    
    @Autowired
    public CardService(final CardDao iDao) {
        super((AbstractDao)iDao);
    }
    
    public List<Card> findAllInRange(final int start, final int max) {
        return (List<Card>)this.dao.findAllInRange(start, max);
    }
}
