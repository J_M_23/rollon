// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.service;

import com.nevreme.rolling.dao.AbstractDao;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.dao.VisitorDao;
import org.springframework.stereotype.Service;
import com.nevreme.rolling.model.Visitor;

@Service
public class VisitorService extends AbstractService<Visitor, Long>
{
    @Autowired
    private VisitorDao visitorDao;
    
    @Autowired
    public VisitorService(final VisitorDao visitorDao) {
        super((AbstractDao)visitorDao);
    }
    
    public Visitor findByVisitorId(final String id) {
        return this.visitorDao.findByVisitorId(id);
    }
}
