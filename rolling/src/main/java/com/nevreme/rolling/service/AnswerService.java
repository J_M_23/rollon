// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.service;

import com.nevreme.rolling.dao.AbstractDao;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.dao.AnswerDao;
import org.springframework.stereotype.Service;
import com.nevreme.rolling.model.Answer;

@Service
public class AnswerService extends AbstractService<Answer, Long>
{
    @Autowired
    private AnswerDao answerDao;
    
    @Autowired
    public AnswerService(final AnswerDao answerDao) {
        super((AbstractDao)answerDao);
    }
}
