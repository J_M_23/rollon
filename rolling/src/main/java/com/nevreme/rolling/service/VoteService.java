// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.service;

import com.nevreme.rolling.dao.AbstractDao;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.dao.VoteDao;
import org.springframework.stereotype.Service;
import com.nevreme.rolling.model.Vote;

@Service
public class VoteService extends AbstractService<Vote, Long>
{
    @Autowired
    private VoteDao voteDao;
    
    @Autowired
    public VoteService(final VoteDao voteDao) {
        super((AbstractDao)voteDao);
    }
    
    public Vote findVoteByVisitorAndAnswer(final Long visitorId, final Long answerId) {
        return this.voteDao.findVoteByVisitorAndAnswer(visitorId, answerId);
    }
}
