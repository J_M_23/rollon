// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.service;

import java.util.List;
import org.springframework.cache.annotation.Cacheable;
import com.nevreme.rolling.dao.AbstractDao;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.dao.PlaylistDao;
import org.springframework.stereotype.Service;
import com.nevreme.rolling.model.Playlist;

@Service
public class PlaylistService extends AbstractService<Playlist, Long>
{
    @Autowired
    PlaylistDao dao;
    
    @Autowired
    public PlaylistService(final PlaylistDao iDao) {
        super((AbstractDao)iDao);
    }
    
    @Cacheable({ "sitecache" })
    public Long getPlaylistByName(final String name) {
        return this.dao.getPlaylistByName(name);
    }
    
    @Cacheable({ "sitecache" })
    public List<Playlist> getPLaylstByType(final int type) {
        return (List<Playlist>)this.dao.getPLaylstByType(type);
    }
    
    @Cacheable({ "sitecache" })
    public List<Playlist> getPLaylstByTypeInRange(final int type, final int start, final int end) {
        return (List<Playlist>)this.dao.getPLaylstByTypeInRange(type, start, end);
    }
    
    @Cacheable({ "sitecache" })
    public List<Playlist> getPLaylstByTypeLazy(final int type) {
        return (List<Playlist>)this.dao.getPLaylstByTypeLazy(type);
    }
    
    @Cacheable({ "sitecache" })
    public Playlist findOneNoTags(final long primaryKey) {
        return this.dao.findOneNoTags(Long.valueOf(primaryKey));
    }
}
