// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.service;

import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.dao.AbstractDao;
import com.nevreme.rolling.dao.LinkSuggestionDao;
import org.springframework.stereotype.Service;
import com.nevreme.rolling.model.LinkSuggestion;

@Service
public class LinkSuggestionService extends AbstractService<LinkSuggestion, Long>
{
    @Autowired
    public LinkSuggestionService(final LinkSuggestionDao iDao) {
        super((AbstractDao)iDao);
    }
}
