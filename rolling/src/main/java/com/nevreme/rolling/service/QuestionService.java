// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.service;

import java.util.List;
import com.nevreme.rolling.dao.AbstractDao;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.dao.QuestionDao;
import org.springframework.stereotype.Service;
import com.nevreme.rolling.model.Question;

@Service("questionService")
public class QuestionService extends AbstractService<Question, Long>
{
    @Autowired
    private QuestionDao questionDao;
    
    @Autowired
    public QuestionService(final QuestionDao questionDao) {
        super((AbstractDao)questionDao);
    }
    
    public List<Question> findAllInRange(final int start, final int max) {
        return (List<Question>)this.questionDao.findAllInRange(start, max);
    }
    
    public Question findOneEagerlyActive() {
        return this.questionDao.findOneEagerlyActive();
    }
}
