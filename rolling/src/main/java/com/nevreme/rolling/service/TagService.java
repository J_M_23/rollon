// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.service;

import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.dao.AbstractDao;
import com.nevreme.rolling.dao.TagDao;
import org.springframework.stereotype.Service;
import com.nevreme.rolling.model.Tag;

@Service
public class TagService extends AbstractService<Tag, Long>
{
    @Autowired
    public TagService(final TagDao iDao) {
        super((AbstractDao)iDao);
    }
}
