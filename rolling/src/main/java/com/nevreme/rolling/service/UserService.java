// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.service;

import com.nevreme.rolling.dao.AbstractDao;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.dao.UserDao;
import org.springframework.stereotype.Service;
import com.nevreme.rolling.model.User;

@Service("userService")
public class UserService extends AbstractService<User, Long>
{
    @Autowired
    private UserDao dao;
    
    @Autowired
    public UserService(final UserDao adminDao) {
        super((AbstractDao)adminDao);
    }
    
    public User findUserByEmail(final String email) {
        return this.dao.findUserByEmail(email);
    }
}
