// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.service;

import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.dao.AbstractDao;
import com.nevreme.rolling.dao.HtmlTemplateDao;
import org.springframework.stereotype.Service;
import com.nevreme.rolling.model.HtmlTemplate;

@Service
public class HtmlTemplateService extends AbstractService<HtmlTemplate, Long>
{
    @Autowired
    public HtmlTemplateService(final HtmlTemplateDao iDao) {
        super((AbstractDao)iDao);
    }
}
