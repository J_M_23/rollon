// 
// Decompiled by Procyon v0.5.36
// 

package com.nevreme.rolling.service;

import java.io.Serializable;
import org.springframework.cache.annotation.Cacheable;
import java.util.List;
import com.nevreme.rolling.dao.AbstractDao;
import org.springframework.beans.factory.annotation.Autowired;
import com.nevreme.rolling.dao.VideoDao;
import org.springframework.stereotype.Service;
import com.nevreme.rolling.model.Video;

@Service("videoService")
public class VideoService extends AbstractService<Video, Long>
{
    @Autowired
    VideoDao dao;
    
    @Autowired
    public VideoService(final VideoDao iDao) {
        super((AbstractDao)iDao);
    }
    
    public Video findVideoByState(final int playing) {
        return this.dao.findVideoByState(playing);
    }
    
    public Video findNextVideo(final int index) {
        return this.dao.findNextVideo(index);
    }
    
    public void insertNewVideo(final Video video) {
        this.dao.insertNewVideo(video);
    }
    
    public List<Video> updateAllHigherThan(final int index, final Long playlist) {
        return (List<Video>)this.dao.updateAllHigherThan(index, playlist);
    }
    
    public void updateList(final List<Video> videos) {
        this.dao.updateList((List)videos);
    }
    
    public Video findVideoByStateForPlaylist(final int state, final Long playlist_id) {
        return this.dao.findVideoByStateForPlaylist(state, playlist_id);
    }
    
    public Video findNextVideoForPlaylist(final int state, final Long playlist_id) {
        return this.dao.findNextVideoForPlaylist(state, playlist_id);
    }
    
    @Cacheable({ "sitecache" })
    public Video findVideoByYtId(final String ytId, final Long plId) {
        return this.dao.findVideoByYtId(ytId, plId);
    }
    
    public List<Video> findVideosForPlaylistByRange(final Long plId, final int start, final int end) {
        return (List<Video>)this.dao.findVideosForPlaylistByRange(plId, start, end);
    }
    
    @Cacheable({ "sitecache" })
    public Video getRecommendedVideo(final int type) {
        return this.dao.findRecommendedVideoByPlaylist(type);
    }
    
    public Video findOne(final Long primaryKey) {
        System.out.println("******ONE*********\n\n\n\n\n\n********ONE*******");
        return (Video)this.dao.findOne(primaryKey);
    }
}
